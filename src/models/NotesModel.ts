export interface NotesModel {
	_key: string
	title: string
	text: string
	user: string
	client: string
	date: Date
}