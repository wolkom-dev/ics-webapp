export interface PrioritisationClientDataModel {
	key: string
	Name: string
	Tier: string
	SegmentSize: string
	Industry: string
	EmployeeCount: number
	engagement: number
	individual: number
	productivity: number
}