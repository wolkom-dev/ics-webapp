export interface PrioritisationIndustryDataModel {
	key: string
	engagement: number
	individual: number
	productivity: number
}