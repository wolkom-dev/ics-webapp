import {CollectionRobin} from '@simplus/robin'
import {UsersModel} from '../models'


export class UsersRobin extends CollectionRobin<UsersModel> {

	/**
	 *  Robin to create new dashboard
	 *  @param {string} key - Key
	 *  @param {string} name - Name
	 * 	@param {string} password - password
	 *
	 */
	createProfile(key: string, name: string, password: string): void {
		this.post(
			'createProfile',
			``,
			{
				'key': key,
				'name': name,
				'password': password
			}
		);
	}
}