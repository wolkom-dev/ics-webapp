import * as React from 'react';

import {NavLink} from 'react-router-dom';
import {connectRobin} from '@simplus/robin-react'
import {robins} from '../../../../robins'
import {withRouter, RouteComponentProps} from 'react-router-dom'
import {HorizontalNavBar, HorizontalNavBarWrapper, HorizontalNavBarItem} from '@simplus/siui';
import { hasPermission } from '../../../../utils';
import {ErrorBoundary} from '../../../../utils/ErrorBoundary'
const {PermissionsRobin} = robins;
const Item = HorizontalNavBarItem;

@connectRobin([PermissionsRobin])
class TopMenu extends React.Component<RouteComponentProps<{id: string}>> {
	render(): JSX.Element {
		return <ErrorBoundary>
				<HorizontalNavBarWrapper
				style={{flex : '1 1 auto'}}
				flexContainer
				menu={<HorizontalNavBar>
					{hasPermission('/view/icas-summary/view-summary', PermissionsRobin.getResult('own-permissions')) ?
					<NavLink activeClassName='active' to={`/analysis/client-prioritisation/${this.props.match.params.id}/summary`}><Item>ICAS SUMMARY</Item></NavLink>
					: null}
					{hasPermission('/view/programme-info', PermissionsRobin.getResult('own-permissions')) ?
					<NavLink activeClassName='active' to={`/analysis/client-prioritisation/${this.props.match.params.id}/programmeinfo`}><Item>PROGRAMME INFO</Item></NavLink>
					: null }
					<NavLink activeClassName='active' to={`/analysis/client-prioritisation/${this.props.match.params.id}/dashboard/`}><Item>DASHBOARD</Item></NavLink>
					{hasPermission('/view/prioritization', PermissionsRobin.getResult('own-permissions')) ?
					<NavLink activeClassName='active' to={`/analysis/client-prioritisation/${this.props.match.params.id}/prioritisation`}><Item>PRIORITISATION</Item></NavLink>
					: null }
					{hasPermission('/view/stories/view-story', PermissionsRobin.getResult('own-permissions')) ?
					<NavLink activeClassName='active' to={`/analysis/client-prioritisation/${this.props.match.params.id}/stories`}><Item>STORIES</Item></NavLink>
					: null }
					{hasPermission('/view/notes/view-note', PermissionsRobin.getResult('own-permissions')) ?
					<NavLink activeClassName='active' to={`/analysis/client-prioritisation/${this.props.match.params.id}/notes`}><Item>NOTES</Item></NavLink>
					: null }
				</HorizontalNavBar>}
			>
					{this.props.children}
			</HorizontalNavBarWrapper>
		</ErrorBoundary>
	}
}
export const TopMenuLayout = withRouter<RouteComponentProps<{id: string}>>(
	TopMenu
)