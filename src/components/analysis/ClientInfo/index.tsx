import * as React from 'react';
import {Route, Switch, Redirect, RouteComponentProps} from 'react-router-dom'
import {ProgrammeInfo} from './ProgrammeInfo'
import {ClientDashboard} from './Dashboard'
import {IcasSummary} from './IcasSummary'
import {Prioritisation} from './Prioritisation'
import {Stories} from './Stories'
import {Notes} from './Notes'
import {TopMenuLayout} from './layout/TopMenuLayout'

export class ClientInfo extends React.Component<RouteComponentProps<{id: string}>> {
	/**
	 *  Render method
	 */
	render(): JSX.Element {
		return(
			<TopMenuLayout>
				<Switch>
					<Redirect exact from='/analysis/client-prioritisation/:id' to={`/analysis/client-prioritisation/${this.props.match.params.id}/programmeinfo`}/>
					<Route path='/analysis/client-prioritisation/:id/summary' component={IcasSummary} />
					<Route path='/analysis/client-prioritisation/:id/dashboard' component={ClientDashboard} />
					<Route path='/analysis/client-prioritisation/:id/programmeinfo' component={ProgrammeInfo} />
					<Route path='/analysis/client-prioritisation/:id/prioritisation/:department?/:key?' component={Prioritisation} />
					<Route path='/analysis/client-prioritisation/:id/stories' component={Stories} />
					<Route path='/analysis/client-prioritisation/:id/notes' component={Notes} />
				</Switch>
			</TopMenuLayout>
		)
	}
}

export default ClientInfo;