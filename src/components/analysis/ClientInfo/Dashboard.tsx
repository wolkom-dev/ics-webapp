import * as React from 'react'
import {HashRouter as Router, Route, Switch, Redirect, RouteComponentProps} from 'react-router-dom'
import {Effectiveness, Health, Roi, Engagement, CostToServe, Quality} from './DashboardContent/index'
export class ClientDashboard extends React.Component<RouteComponentProps<{id: string}>> {

	render(): JSX.Element {
		return(
			<Router>
				<Switch>
					<Redirect exact from='/analysis/client-prioritisation/:id/dashboard' to={`/analysis/client-prioritisation/${this.props.match.params.id}/dashboard/engagement`}/>
					<Route exact path='/analysis/client-prioritisation/:id/dashboard/engagement' component={Engagement} />
					<Route path='/analysis/client-prioritisation/:id/dashboard/health' component={Health} />
					<Route path='/analysis/client-prioritisation/:id/dashboard/effectiveness' component={Effectiveness} />
					<Route path='/analysis/client-prioritisation/:id/dashboard/quality' component={Quality} />
					<Route path='/analysis/client-prioritisation/:id/dashboard/roi' component={Roi} />
					<Route path='/analysis/client-prioritisation/:id/dashboard/cost-to-serve' component={CostToServe} />
				</Switch>
			</Router>
		)
	}

}
export default ClientDashboard;