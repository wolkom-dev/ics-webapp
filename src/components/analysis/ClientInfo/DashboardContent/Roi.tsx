import * as React from 'react'
import {
	DashboardContainer,
	Dashboard,
	DashboardFilter,
	DashboardSection,
	DashboardTab,
	KpiChart,
	BarChart
} from 'src/components/dashboard'
import * as moment from 'moment'
import * as queryString from 'query-string'
import * as _ from 'lodash'
import {Tabs, TabPane, Loader} from '@simplus/siui'
import {notification} from 'antd'
import {connectRobin} from '@simplus/robin-react'
import { RouteComponentProps } from 'react-router-dom'
import {robins} from 'src/robins'
import {DateRangeFilter, numberFormater, currency, relative, shortNumber} from './utils'
import {hasPermission} from '../../../../utils'
import {ErrorBoundary} from '../../../../utils/ErrorBoundary'
const {AnalyticsRobin, clientSettings, PermissionsRobin} = robins

@connectRobin([AnalyticsRobin, clientSettings, PermissionsRobin])
export class Roi extends React.Component<RouteComponentProps<{id: string}>> {
	state = {filter: moment('2018-01-01'), compareBenchmark: true}
	unlisten?: () => void = undefined
	componentWillMount(): void {
		clientSettings.when(clientSettings.findOne(this.props.match.params.id)).then(() => {
			const settings = clientSettings.getModel();
			if (settings[0]) {
				const reportingCycle = moment(settings[0].reportingCycle)
				if (!reportingCycle.isSame(this.state.filter, 'd')) {
					this.setState({filter: reportingCycle})
				}
			}
			this.applyFilters()
		}).catch( err => {
			notification.error({type: 'error', message: 'Could not load settings data !', description: 'There was an error during the settings api execution.'})
		})
		this.unlisten = this.props.history.listen((location, action) => {
			if (action === 'POP')
				this.applyFilters()
		});
	}
	applyFilters(): void {
		const benchmark = queryString.parse(location.hash.split('?')[1]).benchmark || 'Previous range'
		const dateRange = queryString.parse(location.hash.split('?')[1]).dateRange || 'quarter'

		const startDate = queryString.parse(location.hash.split('?')[1]).startDate || moment().startOf('quarter').subtract(1, dateRange).toISOString();
		const endDate = queryString.parse(location.hash.split('?')[1]).endDate || moment().startOf('quarter').toISOString();

		const options: any = {
			startdate : startDate,
			enddate : endDate,
			client : this.props.match.params.id,
			dailyworkhours: 8,
			hourlywage: 120,
			contractvalue : 0
		}
		if (benchmark === 'Previous range') {
			options.benchmarkstartdate = moment(startDate).subtract(1, dateRange).toISOString()
			options.benchmarkenddate = moment(endDate).subtract(1, dateRange).toISOString()
		} else {
			options.benchmark = benchmark.split(',')
		}
		AnalyticsRobin.when(AnalyticsRobin.post('roi_keymetrics', '/roi/keymetrics', {...options})).catch( err => {
			notification.error({type: 'error', message: 'Could not load roi keymetrics data !', description: 'There was an error during the analytics api execution.'})
		})
		AnalyticsRobin.when(AnalyticsRobin.post('roi_products', '/roi/keymetrics', {...options, split : 'product'})).catch( err => {
			notification.error({type: 'error', message: 'Could not load roi products data !', description: 'There was an error during the analytics api execution.'})
		})
	}
	componentWillUnmount(): void {
		if (this.unlisten)
			this.unlisten();
	}
	pageRedirect(update: any, refresh: boolean = false): void {
		const state = {
			...this.urlState(),
			...update}
		this.props.history.push(`${this.props.location.pathname}?${queryString.stringify(state)}`)
		if (refresh)
			this.applyFilters()
	}
	urlState(): any {
		return queryString.parse(location.hash.split('?')[1])
	}
	render(): JSX.Element {
		const keyMetrics = _.get(AnalyticsRobin.getResult('roi_keymetrics'), 'data', {})
		const splitByProducts = _.get(AnalyticsRobin.getResult('roi_products'), 'data', [])
		const clientID = this.props.match.params.id
		const noPermission = <h1 className='no-permission'>You do not have permission to view this page</h1>
		// Query Strings
		const { dateRange, site, benchmark, productbreakdown} = _.defaults(this.urlState(), {
			dateRange : 'quarter',
			site: 'All',
			benchmark: 'Previous range',
			productbreakdown: 'Gains'
		})
		const settings = clientSettings.getModel()
		const siteLevel: Array<{value: string, text: string}> = []
		if (settings && settings[0] && settings[0].siteMapping)
			settings[0].siteMapping.map(item => siteLevel.push({value: item.name, text: item.alias}))

		const compareBenchmark = this.state.compareBenchmark
		const benchmarkMapping = {
			'Previous range': 'Previous period',
			'industry': 'Sector',
			'SegmentSize': 'Size'
		}
		const ROIDashboard = (
			<DashboardContainer style={{backgroundColor: '#F9FBFF', padding: '1rem', position: 'relative'}}>
				{(clientSettings.isLoading(clientSettings.ACTIONS.FIND_ONE)||clientSettings.isError(clientSettings.ACTIONS.FIND_ONE)) ? <Loader error={clientSettings.isError(clientSettings.ACTIONS.FIND_ONE)}/> : null}
				<div className='dashboard-top-bar'>
					<div style={{display: 'flex', padding: '1rem'}}>
						<DashboardFilter
							width={130}
							type='single'
							title='SITE'
							defaultValue={site}
							style={{marginRight: '1rem'}}
							options={[{value: 'All', text: 'All sites'}, ...siteLevel]}
							onChange={(selected) => this.pageRedirect({site: selected}, true)
						}/>
						<DateRangeFilter value={dateRange} customOption hideMonth financialStartYear={moment(this.state.filter)} label='DATE RANGE' style={{minWidth: 130, marginRight: '1rem'}}
							onChange={(selected) => this.pageRedirect({
								dateRange: selected.value,
								startDate: moment(selected.startDate).toISOString(),
								endDate: moment(selected.endDate).toISOString()
							}, true)
						}/>
						<DashboardFilter
							width={150}
							type='single'
							text='Compared to'
							title='BENCHMARK'
							defaultValue={benchmark}
							options={[{value: 'Previous range', text: 'Previous period'}, {value: 'industry', text: 'Sector'}, {value: 'SegmentSize', text: 'Size'}]}
							onChange={(selected) => this.pageRedirect({benchmark: selected}, true)
							}/>
						<DashboardFilter
							type='switch'
							text='Display Benchmark'
							checked={compareBenchmark}
							onChange={(selected) => this.setState({compareBenchmark: selected})
							}/>
						<span style={{alignSelf: 'flex-end', marginLeft: '0.5rem', marginBottom: '0.8rem'}}>Relative</span>
					</div>
				</div>
				<Dashboard
					structure={{
						items : [4, 4],
						size : 4
					}}>
					<DashboardSection section={{
						title : 'Return on investment metrics',
						tooltip : 'Tooltip..'
					}}>
						<DashboardTab>
							<KpiChart
								kpis={[
									{
										title : 'Monetary gains',
										data : shortNumber(keyMetrics.gains, currency),
										loading: AnalyticsRobin.isLoading('roi_keymetrics'),
										error: AnalyticsRobin.isError('roi_keymetrics'),
										footer: compareBenchmark ? `${relative(keyMetrics.gains, keyMetrics.gainsbenchmmark)} Vs Benchmark (${currency(keyMetrics.gainsbenchmmark)})` : ''
									}, {
										title: 'Contract value',
										data : shortNumber(keyMetrics.contractvalue, currency),
										loading: AnalyticsRobin.isLoading('roi_keymetrics'),
										error: AnalyticsRobin.isError('roi_keymetrics'),
									}, {
										title : 'ROI',
										data : numberFormater(keyMetrics.roi),
										loading: AnalyticsRobin.isLoading('roi_keymetrics'),
										error: AnalyticsRobin.isError('roi_keymetrics'),
										footer: compareBenchmark ? `${relative(keyMetrics.roi, keyMetrics.roibenchmark)} Vs Benchmark (${numberFormater(keyMetrics.roibenchmark)})` : ''
									}
								]}
							/>
						</DashboardTab>
					</DashboardSection>
					<DashboardSection section={{
						title : 'Product breakdown',
						tooltip : 'Tooltip..'
					}}>
						<DashboardTab>
						<div style={{display: 'flex', alignItems: 'center'}}>
							<DashboardFilter width={150}
							type='single'
							defaultValue={productbreakdown}
							options={[
								{value: 'Gains', text: 'Monetary Gains'},
								{value: 'ROI', text: 'ROI'}]}
							onChange={(selected) => this.pageRedirect({productbreakdown: selected}, true)
							}/>
							<span style={{marginLeft: '1rem'}}> split by product</span>
							</div>
							<BarChart
								horizontal={true}
								bars={compareBenchmark ? [productbreakdown, `${benchmarkMapping[benchmark]} - ${productbreakdown} benchmark` ] : [productbreakdown]}
								loading={AnalyticsRobin.isLoading('roi_products')}
								error={AnalyticsRobin.isError('roi_products')}
								config={{showLegend: compareBenchmark}}
								data={splitByProducts.map( k => {
									return { x : k.key,
										Gains: k.gains,
										'Previous period - Gains benchmark': k.gainsbenchmmark,
										'Sector - Gains benchmark': k.gainsbenchmmark,
										'Size - Gains benchmark': k.gainsbenchmmark,
										'ROI': k.roi,
										'Previous period - ROI benchmark': k.roibenchmark,
										'Sector - ROI benchmark': k.roibenchmark,
										'Size - ROI benchmark': k.roibenchmark
									}
								})}
							/>
						</DashboardTab>
					</DashboardSection>
				</Dashboard>
			</DashboardContainer>
		)
		return(
			<ErrorBoundary>
				<Tabs selectedDefault={4} fillContainer className= 'tab' style={{fontWeight: 'bold'}}
				active={(current) => {
					switch (current) {
						case 0:
							this.props.history.push(`/analysis/client-prioritisation/${clientID}/dashboard/engagement`)
							break;
						case 1:
							this.props.history.push(`/analysis/client-prioritisation/${clientID}/dashboard/health`)
							break;
						case 2:
							this.props.history.push(`/analysis/client-prioritisation/${clientID}/dashboard/effectiveness`)
							break;
						case 3:
							this.props.history.push(`/analysis/client-prioritisation/${clientID}/dashboard/quality`)
							break;
						case 5:
							this.props.history.push(`/analysis/client-prioritisation/${clientID}/dashboard/cost-to-serve`)
							break;
					}
				}}
				>
					<TabPane label='Engagement' disabled={!hasPermission('/view/dashboard/engagement', PermissionsRobin.getResult('own-permissions')) }/>
					<TabPane label='Health' disabled={!hasPermission('/view/dashboard/health', PermissionsRobin.getResult('own-permissions'))}/>
					<TabPane label='Effectiveness' disabled={!hasPermission('/view/dashboard/effectiveness', PermissionsRobin.getResult('own-permissions'))}/>
					<TabPane label='Quality' disabled={!hasPermission('/view/dashboard/quality', PermissionsRobin.getResult('own-permissions'))}/>
					<TabPane label='ROI'>
						{hasPermission('/view/dashboard/roi', PermissionsRobin.getResult('own-permissions')) ? ROIDashboard : noPermission}
					</TabPane>
					<TabPane label='CostToServe' disabled={!hasPermission('/view/dashboard/cost-to-serve', PermissionsRobin.getResult('own-permissions'))}/>
				</Tabs>
			</ErrorBoundary>
		)
	}
}
export default Roi;