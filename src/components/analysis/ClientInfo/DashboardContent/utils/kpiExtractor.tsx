import * as _ from 'lodash'
import { isArray } from 'util';

export function extractKpis(kpis: string[], data: any = {}, percent: boolean = false): {[k: string] : {value : number, benchmark : number}} {
	const res = {}

	kpis.forEach( k => {
		if (percent)
			res[k] = { value :  _.get(data, `data.${k}.[0].value`, '?') * 100, benchmark : _.get(data, `data.${k}.[0].benchmark`, '?') * 100 }
		else
			res[k] = { value :  _.get(data, `data.${k}.[0].value`, '?'), benchmark : _.get(data, `data.${k}.[0].benchmark`, '?') }
	})
	return res;
}


export function extractKpisArrays(kpis: string[], data: any = {}, percent: boolean = false): {[k: string] : {x: string, value : number, benchmark : number}[]} {
	const res = {}

	kpis.forEach( k => {
		let raw = _.get(data, `data.${k}`)
		if(isArray(raw)){
			if (percent)
				res[k] = raw.map( r => ({value : r.value * 100, benchmark : r.benchmark * 100, x : r.key}))
			else
				res[k] = raw.map( r => ({value : r.value, benchmark : r.benchmark, x : r.key}))
		} else {
			res[k] = []
		}
	})
	return res;
}