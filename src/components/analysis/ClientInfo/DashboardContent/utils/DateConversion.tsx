import * as moment from 'moment'
export interface DatesObject {
	startDate: moment.Moment
	endDate: moment.Moment
}
export function getPastMonth(financialYearStartDate: moment.Moment = moment('2001-01-01')): DatesObject {
	const currentDate = moment()
	const currentYearStartDate = moment(financialYearStartDate).year(currentDate.year())
	if (currentYearStartDate.isAfter(currentDate))
		currentYearStartDate.year(currentDate.year() - 1)
	let pastMonth = moment(currentYearStartDate).subtract(1, 'month')
	let output
	for (let i = 0; i < 12 ; i++) {
		const currentMonth = moment(currentYearStartDate).add(i, 'months')
		if (currentDate.isBetween(pastMonth, currentMonth)) {
			output =  {
				startDate: moment(pastMonth).subtract(1, 'M'),
				endDate: pastMonth
			}
		}
		pastMonth = moment(currentMonth)
	}
	return output
}
export function getPastQuarter(financialYearStartDate: moment.Moment = moment('2001-01-01')): DatesObject {
	const currentDate = moment()
	const currentYearStartDate = moment(financialYearStartDate).year(currentDate.year())
	if (currentYearStartDate.isAfter(currentDate))
		currentYearStartDate.year(currentDate.year() - 1)
	const past_Quarter =  {startDate: moment(currentYearStartDate).subtract(1, 'quarter'), endDate: currentYearStartDate}
	const Q1 = {startDate: currentYearStartDate, endDate: moment(currentYearStartDate).add(1, 'quarter')}
	const Q2 = {startDate: moment(Q1.endDate), endDate: moment(Q1.endDate).add(1, 'quarter')}
	const Q3 = {startDate: moment(Q2.endDate), endDate: moment(Q2.endDate).add(1, 'quarter')}
	const Q4 = {startDate: moment(Q3.endDate), endDate: moment(Q3.endDate).add(1, 'quarter')}

	if (currentDate.isBetween(Q1.startDate, Q1.endDate))
		return past_Quarter
	else if (currentDate.isBetween(Q2.startDate, Q2.endDate))
		return Q1
	else if (currentDate.isBetween(Q3.startDate, Q3.endDate))
		return Q2
	else (currentDate.isBetween(Q4.startDate, Q4.endDate))
		return Q3

}
export function getPastYear(financialYearStartDate: moment.Moment = moment('2001-01-01')): DatesObject {
	const currentDate = moment()
	const currentYearStartDate = moment(financialYearStartDate).year(currentDate.year())
	if (currentYearStartDate.isAfter(currentDate))
		currentYearStartDate.year(currentDate.year() - 1)

	const pastYear = {startDate: moment(currentYearStartDate).subtract(1, 'year'), endDate: currentYearStartDate}
	return pastYear
}
export function getPreviousMonth(financialYearStartDate: moment.Moment = moment('2001-01-01')): DatesObject {
	const pastMonth = getPastMonth(financialYearStartDate)
	const prevMonth = {startDate: pastMonth.startDate.subtract(1, 'month'), endDate: pastMonth.endDate.subtract(1, 'month')}
	return prevMonth
}
export function getPreviousQuarter(financialYearStartDate: moment.Moment = moment('2001-01-01')): DatesObject {
	const pastQuarter = getPastQuarter(financialYearStartDate)
	const prevQuarter = {startDate: pastQuarter.startDate.subtract(1, 'quarter'), endDate: pastQuarter.endDate.subtract(1, 'quarter')}
	return prevQuarter
}
export function getPreviousYear(financialYearStartDate: moment.Moment = moment('2001-01-01')): DatesObject {
	const pastYear = getPastYear(financialYearStartDate)
	const prevYear = {startDate: pastYear.startDate.subtract(1, 'year'), endDate: pastYear.endDate.subtract(1, 'year')}
	return prevYear
}