import * as React from 'react'
import {notification} from 'antd'
import {
	DashboardContainer,
	Dashboard,
	DashboardSection,
	DashboardTab,
	DashboardFilter,
	KpiChart,
	BarChart,
	TableChart
} from 'src/components/dashboard'
import * as _ from 'lodash'
import * as moment from 'moment'
import * as queryString from 'query-string'
import {Tabs, TabPane, Loader} from '@simplus/siui'
import { RouteComponentProps } from 'react-router-dom';
import {
	DateRangeFilter,
	shortNumber,
	currency,
	numberFormater,
	relative,
	percentage
} from './utils'

import {hasPermission} from '../../../../utils'
import {ErrorBoundary} from '../../../../utils/ErrorBoundary'
import {connectRobin} from '@simplus/robin-react'
import {robins} from 'src/robins'
const {AnalyticsRobin, ProblemsRobin, ProblemClustersRobin, clientSettings, PermissionsRobin} = robins

@connectRobin([AnalyticsRobin, ProblemsRobin, ProblemClustersRobin, clientSettings, PermissionsRobin])
export class Health extends React.Component<RouteComponentProps<{id: string}>> {
	state = {filter: moment('2018-01-01'), compareBenchmark:  true, associations : []}
	unlisten?: () => void = undefined
	componentWillMount(): void {
		ProblemsRobin.find({})
		ProblemClustersRobin.find({})
		clientSettings.when(clientSettings.findOne(this.props.match.params.id)).then(() => {
			const settings = clientSettings.getModel();
			if (settings[0]) {
				const reportingCycle = moment(settings[0].reportingCycle)
				if (!reportingCycle.isSame(this.state.filter, 'd')) {
					this.setState({filter: reportingCycle})
				}
			}
			this.applyFilters()
		}).catch( err => {
			notification.error({type: 'error', message: 'Could not load settings data !', description: 'There was an error during the settings api execution.'})
		})
		this.unlisten = this.props.history.listen((location, action) => {
			if (action === 'POP')
				this.applyFilters()
		});
	}
	applyFilters(): void {
		const benchmark = queryString.parse(location.hash.split('?')[1]).benchmark || 'Previous range'
		const dateRange = queryString.parse(location.hash.split('?')[1]).dateRange || 'month'

		const startDate = queryString.parse(location.hash.split('?')[1]).startDate || moment().startOf('month').subtract(1, dateRange).toISOString();
		const endDate = queryString.parse(location.hash.split('?')[1]).endDate || moment().startOf('month').toISOString();

		const options: any = {
			startdate : startDate,
			enddate : endDate,
			client : this.props.match.params.id,
			dailyworkhours: 8,
			hourlywage: 120,
		}

		if (benchmark === 'Previous range') {
			options.benchmarkstartdate = moment(startDate).subtract(1, dateRange).toISOString()
			options.benchmarkenddate = moment(endDate).subtract(1, dateRange).toISOString()
		} else {
			options.benchmark = benchmark.split(',')
		}
		AnalyticsRobin.when(AnalyticsRobin.post('health_prioritization', '/health/prioritization', {...options})).catch(err => {
			notification.error({type: 'error', message: 'Could not load health prioritization data !', description: 'There was an error during the analytics api execution.'})
		})
		AnalyticsRobin.when(AnalyticsRobin.post('health_prioritization_drilldown', '/health/prioritization', {...options, split : 'problem'})).catch(err => {
			notification.error({type: 'error', message: 'Could not load health prioritization drilldown data !', description: 'There was an error during the analytics api execution.'})
		})
		AnalyticsRobin.when(AnalyticsRobin.post('health_keymetrics', '/health/keymetrics', options)).catch(err => {
			notification.error({type: 'error', message: 'Could not load health keymetrics data !', description: 'There was an error during the analytics api execution.'})
		})
		AnalyticsRobin.when(AnalyticsRobin.post('health_breakdown', '/health/breakdown', {...options, casetype: this.urlState().breakdownFilterOn || '', split : this.urlState().problemsplit || 'problemcluster'})).then( key => {
			this.setState({associations : AnalyticsRobin.getResult(key).data.map( d => {
				return { value : d.key, text : [d.key] }
			})})
		}).catch(err => {
			notification.error({type: 'error', message: 'Could not load health drill down data !', description: 'There was an error during the analytics api execution.'})
		})
		AnalyticsRobin.when(AnalyticsRobin.post('health_association', '/health/associations', {...options, casetype: this.urlState().breakdownFilterOn || '', split : this.urlState().association || 'problemcluster'})).catch(err => {
			notification.error({type: 'error', message: 'Could not load health drill down data !', description: 'There was an error during the analytics api execution.'})
		})
		AnalyticsRobin.when(AnalyticsRobin.post('health_drill_down', '/health/breakdown', {...options, casetype: this.urlState().breakdownFilterOn || '', split : this.urlState().drilldownsplit || 'severity'})).catch(err => {
			notification.error({type: 'error', message: 'Could not load health drill down data !', description: 'There was an error during the analytics api execution.'})
		})
	}

	componentWillUnmount(): void {
		if (this.unlisten)
			this.unlisten();
	}

	pageRedirect(update: any, refrsh: boolean = false): void {
		const state = {
			...this.urlState(),
			...update}
		this.props.history.push(`${this.props.location.pathname}?${queryString.stringify(state)}`)
		if (refrsh)
			this.applyFilters()
	}

	urlState(): any {
		return queryString.parse(location.hash.split('?')[1])
	}


	render(): JSX.Element {
		const clientID = this.props.match.params.id
		const noPermission = <h1 className='no-permission'>You do not have permission to view this page</h1>
		// Query Strings
		const { dateRange, site, benchmark, problemsplit, problembreakdown, drilldownsplit, comparison, tab} = _.defaults(this.urlState(), {
			dateRange : 'month',
			site: 'All',
			benchmark: 'Previous range',
			problemsplit: 'problemcluster',
			drilldownsplit: 'severity',
			comparison: 'absolute',
			tab: '0/.0',
			breakdownFilterOn : ''
		})
		const displayRelativeValues = comparison === 'relative'
		// Robin results
		const problemList: Array<{value: string, text: string}> = []
		ProblemsRobin.getCollection().map(item => problemList.push({value: item.problem, text: item.problem}))
		const problemClusterList: Array<{value: string, text: string}> = []
		ProblemClustersRobin.getCollection().map(item => problemClusterList.push({value: item.problemcluster, text: item.problemcluster}))
		const settings = clientSettings.getModel()
		const siteLevel: Array<{value: string, text: string}> = []
		if (settings && settings[0] && settings[0].siteMapping)
			settings[0].siteMapping.map(item => siteLevel.push({value: item.name, text: item.alias}))

		const kpis = {...(AnalyticsRobin.getResult('health_keymetrics') || { data : {}}).data}
		const table = (AnalyticsRobin.getResult('health_prioritization_drilldown') || { data : []}).data
		const association = ((AnalyticsRobin.getResult('health_association') || { data : [] }).data || []).map( k => ({...k}))
		const breakdown = ((AnalyticsRobin.getResult('health_breakdown') || { data : [] }).data || []).map( k => ({...k}))
		const drillDown = ((AnalyticsRobin.getResult('health_drill_down') || { data : [] }).data || []).map( k => ({...k}))

		if (comparison !== 'absolute') {
			[kpis, breakdown, drillDown].forEach( o => {
				const os = Array.isArray(o) ? o : [o]
				os.forEach( e => {
					_.keys(e).filter( k => /relative$/.test(k)).map( k => k.replace(/relative$/, '')).forEach( k => {
						e[k] = e[`${k}relative`] * 100
					})
				})
			})
		}
		const numberFormaterConditional = displayRelativeValues ? percentage : numberFormater
		const shortNumberFormaterConditional = displayRelativeValues ? percentage : shortNumber
		const compareBenchmark = this.state.compareBenchmark && !displayRelativeValues
		const benchmarkMapping = {
			'Previous range': 'Previous period',
			'industry': 'Sector',
			'SegmentSize': 'Size'
		}
		const healthDashboard = (
			<DashboardContainer style={{backgroundColor: '#F9FBFF', padding: '1rem', position: 'relative'}}>
				{(clientSettings.isLoading(clientSettings.ACTIONS.FIND_ONE)||clientSettings.isError(clientSettings.ACTIONS.FIND_ONE)) ? <Loader error={clientSettings.isError(clientSettings.ACTIONS.FIND_ONE)}/> : null}
				<div className='dashboard-top-bar'>
					<div style={{display: 'flex', padding: '1rem'}}>
						<DashboardFilter
							width={130}
							type='single'
							title='SITE'
							defaultValue={site}
							style={{marginRight: '1rem'}}
							options={[{value: 'All', text: 'All sites'}, ...siteLevel]}
							onChange={(selected) =>  this.pageRedirect({site: selected}, true)}
							/>
						<DateRangeFilter value={dateRange} customOption financialStartYear={moment(this.state.filter)} label='DATE RANGE' style={{minWidth: 130, marginRight: '1rem'}}
							onChange={(selected) => this.pageRedirect({
								dateRange: selected.value,
								startDate: moment(selected.startDate).toISOString(),
								endDate: moment(selected.endDate).toISOString()
							}, true)
						}/>
						<DashboardFilter
							width={150}
							type='single'
							text='Compared to'
							title='BENCHMARK'
							defaultValue={benchmark}
							options={[{value: 'Previous range', text: 'Previous period'}, {value: 'industry', text: 'Sector'}, {value: 'SegmentSize', text: 'Size'}]}
							onChange={(selected) => this.pageRedirect({benchmark: selected}, true)
							}/>
						<DashboardFilter
							type='switch'
							text='Display Benchmark'
							checked={compareBenchmark}
							onChange={(selected) => this.setState({compareBenchmark: selected})
							}/>
						<DashboardFilter
									type='switch'
									text='Absolute'
									checked={comparison !== 'absolute'}
									onChange={(selected) => this.pageRedirect({comparison: selected ? 'relative' : 'absolute'}, true)
									}/>
								<span style={{alignSelf: 'flex-end', marginLeft: '0.5rem', marginBottom: '0.8rem'}}>Relative</span>
					</div>
				</div>
				<Dashboard
					structure={{
						items : [2, 2, 4, 4],
						size : 4
					}}>
					<DashboardSection
					section={{
						title : 'Work impact score',
						tooltip : 'Tooltip..'
					}}>
						<DashboardTab>
							<KpiChart
								config={{}}
								kpis={[
									{
										loading: AnalyticsRobin.isLoading('health_keymetrics'),
										error: AnalyticsRobin.isError('health_keymetrics'),
										title : 'Work impact score',
										data : numberFormaterConditional(kpis.workimpact),
										footer: compareBenchmark ? `${relative(kpis.workimpact, kpis.workimpactbenchmark)} Vs Benchmark (${numberFormater(kpis.workimpactbenchmark)})` : ''
									},
									{
										loading: AnalyticsRobin.isLoading('health_keymetrics'),
										error: AnalyticsRobin.isError('health_keymetrics'),
										title : 'Working hours at risk',
										data : shortNumberFormaterConditional(kpis.workingriskhours),
										footer : compareBenchmark ? `${relative(kpis.workingriskhours, kpis.workingriskhoursbenchmark)} Vs Benchmark (${shortNumber(kpis.workingriskhoursbenchmark)})` : ''
									},
									{
										loading: AnalyticsRobin.isLoading('health_keymetrics'),
										error: AnalyticsRobin.isError('health_keymetrics'),
										title : 'Work-Days at risk',
										data : numberFormaterConditional(kpis.mandaysatrisk),
										footer : compareBenchmark ? `${relative(kpis.mandaysatrisk, kpis.mandaysatriskbenchmark)} Vs Benchmark (${numberFormater(kpis.mandaysatriskbenchmark)})` : ''
									}
							]}
							/>
						</DashboardTab>
					</DashboardSection>
					<DashboardSection
					section={{
						title : 'High risk factors',
						tooltip : 'Tooltip..'
					}}>
						<DashboardTab>
							<KpiChart
								config={{}}
								kpis={[
									{
										loading: AnalyticsRobin.isLoading('health_keymetrics'),
										error: AnalyticsRobin.isError('health_keymetrics'),
										title : 'Costs of health',
										data : shortNumberFormaterConditional(kpis.costofhealth, displayRelativeValues ? undefined : currency ),
										footer : compareBenchmark ? `${relative(kpis.costofhealth, kpis.costofhealthbenchmark)} Vs Benchmark (${shortNumber(kpis.costofhealthbenchmark, currency)})` : ''
									},
									{
										loading: AnalyticsRobin.isLoading('health_keymetrics'),
										error: AnalyticsRobin.isError('health_keymetrics'),
										title : 'Risk cases',
										data : numberFormaterConditional(kpis.riskcases),
										footer : compareBenchmark ? `${relative(kpis.riskcases, kpis.riskcasesbenchmark)} Vs Benchmark (${numberFormater(kpis.riskcasesbenchmark)})` : ''
									},
									{
										loading: AnalyticsRobin.isLoading('health_keymetrics'),
										error: AnalyticsRobin.isError('health_keymetrics'),
										title : 'Formal referrals',
										data : numberFormaterConditional(kpis.formalreferals),
										footer : compareBenchmark ? `${relative(kpis.formalreferals, kpis.formalreferalsbenchmark)} Vs Benchmark (${numberFormater(kpis.formalreferalsbenchmark)})` : ''
									}
							]}
							/>
						</DashboardTab>
					</DashboardSection>
					<DashboardSection
					defaultActiveTab={tab}
					onTabChange={(selected) => this.pageRedirect({tab: selected})}
					section={{
						tabTitles : ['Problem breakdown', 'Problem associations', 'Problem analysis breakdown', 'Problem Prioritisation'],
						title : 'Problem analysis',
						tooltip : 'Tooltip..'
					}}>
						<DashboardTab>
						<div style={{display: 'flex'}}>
							<DashboardFilter
								width={120}
								type='single'
								text='Display problem proportion split by'
								defaultValue={problemsplit}
								options={[
									{value: 'problem', text: 'Problem'},
									{value: 'problemcluster', text: 'Problem Cluster'}
								]}
								onChange={(selected) => this.pageRedirect({problemsplit: selected}, true)
								}/>
							<DashboardFilter
								width={150}
								type='single'
								text='Filter on'
								defaultValue={this.urlState().breakdownFilterOn || ''}
								options={[
									{value: '', text: 'All Cases'},
									{value: 'riskcases', text: 'Risk Cases'},
									{value: 'formalreferals', text: 'Formal Referals'},
									{value: 'severecases', text: 'Severe Cases'},
									{value: 'significantcases', text: 'Significant Cases'}
								]}
								onChange={(selected) => this.pageRedirect({breakdownFilterOn: selected}, true)
								}/>
						</div>
						<BarChart
								loading={AnalyticsRobin.isLoading('health_breakdown')}
								error={AnalyticsRobin.isError('health_breakdown')}
								bars={compareBenchmark ? ['Actual', `${benchmarkMapping[benchmark]} benchmark`] : ['Actual']}
								config={{showLegend: this.state.compareBenchmark}}
								data={breakdown.sort((a, b) => b.breakdown - a.breakdown).map( k => {
									return {
										x: k.key,
										'Actual': k.breakdown,
										'Previous period benchmark':  k.breakdownbenchmark,
										'Sector benchmark': k.breakdownbenchmark,
										'Size benchmark': k.breakdownbenchmark,
									}
								})}
								relativeToTotal
							/>
						</DashboardTab>
						<DashboardTab>
						<div style={{display: 'flex'}}>
							<DashboardFilter
								width={120}
								type='single'
								text='Display problem proportion split by'
								defaultValue={problemsplit}
								options={[
									{value: 'problem', text: 'Problem'},
									{value: 'problemcluster', text: 'Problem Cluster'}
								]}
								onChange={(selected) => this.pageRedirect({problemsplit: selected}, true)
								}/>
							<DashboardFilter
								width={150}
								type='single'
								text='Filter on'
								defaultValue={this.urlState().breakdownFilterOn || ''}
								options={[
									{value: '', text: 'All Cases'},
									{value: 'riskcases', text: 'Risk Cases'},
									{value: 'formalreferals', text: 'Formal Referals'},
									{value: 'severecases', text: 'Severe Cases'},
									{value: 'significantcases', text: 'Significant Cases'}
								]}
								onChange={(selected) => this.pageRedirect({breakdownFilterOn: selected}, true)
								}/>
							<DashboardFilter
								width={200}
								type='single'
								text='associations for '
								options={this.state.associations}
								defaultValue={problembreakdown}
								onChange={(selected) => this.pageRedirect({problembreakdown: selected}, true)
								}/>
						</div>
						<BarChart
								loading={AnalyticsRobin.isLoading('health_association')}
								error={AnalyticsRobin.isError('health_association')}
								bars={['Actual']}
								config={{showLegend: compareBenchmark}}
								data={association.sort((a, b) => b.value - a.value).map( a => ({
									x : a.key,
									'Actual': a.value
								}))}
								relativeToTotal
							/>
						</DashboardTab>
						<DashboardTab>
						<div style={{display: 'flex'}}>
						<DashboardFilter
								width={120}
								type='single'
								text='At'
								defaultValue={problemsplit}
								options={[
									{value: 'problem', text: 'Problem'},
									{value: 'problemcluster', text: 'Problem Cluster'}
								]}
								onChange={(selected) => this.pageRedirect({problemsplit: selected}, true)
								}/>
							<DashboardFilter
								width={200}
								type='single'
								text='level, drill down into '
								defaultValue={problembreakdown}
								options={this.state.associations}
								onChange={(selected) => this.pageRedirect({problembreakdown: selected}, true)
								}/>
							<DashboardFilter
								width={150}
								type='single'
								text='Filter on'
								defaultValue={this.urlState().breakdownFilterOn || ''}
								options={[
									{value: '', text: 'All Cases'},
									{value: 'riskcases', text: 'Risk Cases'},
									{value: 'formalreferals', text: 'Formal Referals'},
									{value: 'severecases', text: 'Severe Cases'},
									{value: 'significantcases', text: 'Significant Cases'}
								]}
								onChange={(selected) => this.pageRedirect({breakdownFilterOn: selected}, true)
								}/>
							<DashboardFilter
								width={150}
								type='single'
								text='split by'
								defaultValue={drilldownsplit}
								options={[
									{value: 'severity', text: 'Severity'},
									{value: 'employementlvl', text: 'Employment Level'},
									{value: 'gender', text: 'Gender'},
									{value: 'age', text: 'Age'},
									{value: 'referaltype', text: 'Referal Type'},
								]}
								onChange={(selected) => this.pageRedirect({drilldownsplit: selected}, true)
								}/>
						</div>
							<BarChart
								loading={AnalyticsRobin.isLoading('health_drill_down')}
								error={AnalyticsRobin.isError('health_drill_down')}
								bars={compareBenchmark ? ['Actual', `${benchmarkMapping[benchmark]} benchmark`] : ['Actual']}
								config={{showLegend: compareBenchmark}}
								data={drillDown.sort((a, b) => b.breakdown - a.breakdown).map( k => {
									return {
										x: k.key,
										'Actual': k.breakdown,
										'Previous period benchmark':  k.breakdownbenchmark,
										'Sector benchmark':  k.breakdownbenchmark,
										'Size benchmark':  k.breakdownbenchmark
									}
								})}
								relativeToTotal
							/>
						</DashboardTab>
						<DashboardTab>
							<TableChart sortable={true} columns={[
								{ Header : 'Problem type', accessor: 'key'},
								{ Header : 'Risk Hours (benchmark)', accessor: 'riskHoursBenchmark', className: 'center'},
								{ Header : 'Risk Hours (actuals)', accessor: 'riskHours', className: 'center'},
								{ Header : 'Cost of health', accessor: 'costofhealth', className: 'center'},
								{ Header : 'Productivity', accessor: 'productivity', className: 'center'},
							]} data={(table || []).map( (e, i) => ({
								key : e.key,
								riskHoursBenchmark : numberFormater(e.riskhoursbenchmark),
								riskHours : numberFormater(e.riskhours),
								costofhealth : currency(e.costofhealth),
								productivity : percentage(e.productivity * 100)}))}/>
						</DashboardTab>
					</DashboardSection>
				</Dashboard>
			</DashboardContainer>
		)

		return(
			<ErrorBoundary>
				<Tabs selectedDefault={1} fillContainer className= 'tab' style={{fontWeight: 'bold'}}
				active={(current) => {
					switch (current) {
						case 0:
							this.props.history.push(`/analysis/client-prioritisation/${clientID}/dashboard/engagement`)
							break;
						case 2:
							this.props.history.push(`/analysis/client-prioritisation/${clientID}/dashboard/effectiveness`)
							break;
						case 3:
							this.props.history.push(`/analysis/client-prioritisation/${clientID}/dashboard/quality`)
							break;
						case 4:
							this.props.history.push(`/analysis/client-prioritisation/${clientID}/dashboard/roi`)
							break;
						case 5:
							this.props.history.push(`/analysis/client-prioritisation/${clientID}/dashboard/cost-to-serve`)
							break;
					}
				}}
				>
					<TabPane label='Engagement' disabled={!hasPermission('/view/dashboard/engagement', PermissionsRobin.getResult('own-permissions')) }/>
					<TabPane label='Health'>
						{hasPermission('/view/dashboard/health', PermissionsRobin.getResult('own-permissions')) ? healthDashboard : noPermission}
					</TabPane>
					<TabPane label='Effectiveness' disabled={!hasPermission('/view/dashboard/effectiveness', PermissionsRobin.getResult('own-permissions'))}/>
					<TabPane label='Quality' disabled={!hasPermission('/view/dashboard/quality', PermissionsRobin.getResult('own-permissions'))}/>
					<TabPane label='ROI' disabled={!hasPermission('/view/dashboard/roi', PermissionsRobin.getResult('own-permissions'))}/>
					<TabPane label='CostToServe' disabled={!hasPermission('/view/dashboard/cost-to-serve', PermissionsRobin.getResult('own-permissions'))}/>
				</Tabs>
			</ErrorBoundary>
		)
	}

}
export default Health;