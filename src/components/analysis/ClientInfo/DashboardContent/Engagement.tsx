import * as React from 'react'
import {connectRobin} from '@simplus/robin-react'
import * as moment from 'moment'
import {notification} from 'antd'
import {Tabs, TabPane, Loader} from '@simplus/siui'
import { RouteComponentProps } from 'react-router-dom';
import * as _ from 'lodash'
import * as queryString from 'query-string'
import {
	DateRangeFilter,
	percentage,
	numberFormater,
	relative
} from './utils'

import {robins} from 'src/robins'
import {hasPermission} from '../../../../utils'
import {ErrorBoundary} from '../../../../utils/ErrorBoundary'
const {AnalyticsRobin, clientSettings, PermissionsRobin} = robins
import {
	DashboardContainer,
	Dashboard,
	DashboardSection,
	DashboardFilter,
	DashboardTab,
	KpiChart,
	BarChart
} from 'src/components/dashboard'

@connectRobin([AnalyticsRobin, clientSettings, PermissionsRobin])
export class Engagement extends React.Component<RouteComponentProps<{id: string}>> {
	state = {filter: moment('2018-01-01'), compareBenchmark:  true}
	unlisten?: () => void = undefined
	componentWillMount(): void {
		clientSettings.when(clientSettings.findOne(this.props.match.params.id)).then(() => {
			const settings = clientSettings.getModel();
			if (settings[0]) {
				const reportingCycle = moment(settings[0].reportingCycle)
				if (!reportingCycle.isSame(this.state.filter, 'd')) {
					this.setState({filter: reportingCycle})
				}
			}
			this.applyFilters()
		}).catch( err => {
			notification.error({type: 'error', message: 'Could not load settings data !', description: 'There was an error during the settings api execution.'})
		})
		this.unlisten = this.props.history.listen((location, action) => {
			if (action === 'POP')
				this.applyFilters()
		});
	}
	applyFilters(): void {
		const benchmark = queryString.parse(location.hash.split('?')[1]).benchmark || 'Previous range'
		const dateRange = queryString.parse(location.hash.split('?')[1]).dateRange || 'month'

		const startDate = queryString.parse(location.hash.split('?')[1]).startDate || moment().startOf('month').subtract(1, dateRange).toISOString();
		const endDate = queryString.parse(location.hash.split('?')[1]).endDate || moment().startOf('month').toISOString();

		const options: any = {
			startdate : startDate,
			enddate : endDate,
			client : this.props.match.params.id,
			dailyworkhours: 8,
			hourlywage: 120,
		}

		if (benchmark === 'Previous range') {
			options.benchmarkstartdate = moment(startDate).subtract(1, dateRange).toISOString()
			options.benchmarkenddate = moment(endDate).subtract(1, dateRange).toISOString()
		} else {
			options.benchmark = benchmark.split(',')
		}
		AnalyticsRobin.when(AnalyticsRobin.post('engagement_keymetrics', '/engagement/keymetrics', options)).catch( err => {
			notification.error({type: 'error', message: 'Could not load engagement keymetrics data !', description: 'There was an error during the analytics api execution.'})
		})
		AnalyticsRobin.when(AnalyticsRobin.post('engagement_utilization', '/engagement/utilization', options)).catch( err => {
			notification.error({type: 'error', message: 'Could not load engagement utilisation data !', description: 'There was an error during the analytics api execution.'})
		})
		AnalyticsRobin.when(AnalyticsRobin.post('utilization_breakdown', '/engagement/utilization', {...options, split : queryString.parse(location.hash.split('?')[1]).utilizationbreakdown || 'product'})).catch( err => {
			notification.error({type: 'error', message: 'Could not load utilisation breakdown data !', description: 'There was an error during the analytics api execution.'})
		})
		AnalyticsRobin.when(AnalyticsRobin.post('sessions_breakdown', '/engagement/sessions', {...options, split : queryString.parse(location.hash.split('?')[1]).utilizationbreakdown || 'product'})).catch( err => {
			notification.error({type: 'error', message: 'Could not load sessions breakdown data !', description: 'There was an error during the analytics api execution.'})
		})
		AnalyticsRobin.when(AnalyticsRobin.post('engagement_breakdown', '/engagement/keymetrics', {...options, split : queryString.parse(location.hash.split('?')[1]).keymetrics || 'service'})).catch( err => {
			notification.error({type: 'error', message: 'Could not load engagement breakdown data !', description: 'There was an error during the analytics api execution.'})
		})
	}
	componentWillUnmount(): void {
		if (this.unlisten)
			this.unlisten();
	}
	pageRedirect(update: any, refrsh: boolean = false): void {
		const state = {
			...this.urlState(),
			...update}
		this.props.history.push(`${this.props.location.pathname}?${queryString.stringify(state)}`)
		if (refrsh)
			this.applyFilters()
	}

	urlState(): any {
		return queryString.parse(location.hash.split('?')[1])
	}

	render(): JSX.Element {
		const clientID = this.props.match.params.id
		const noPermission = <h1 className='no-permission'>You do not have permission to view this page</h1>
		// Query Strings
		const { dateRange, site, benchmark, utilizationbreakdown, keymetrics, comparison, tab} = _.defaults(this.urlState(), {
			dateRange : 'month',
			site: 'All',
			benchmark: 'Previous range',
			utilizationbreakdown: 'product',
			keymetrics: 'service',
			comparison: 'absolute',
			tab: '0/.0'
		})
		const displayRelativeValues = comparison === 'relative'
		// Robin results
		const settings = clientSettings.getModel()
		const siteLevel: Array<{value: string, text: string}> = []
		if (settings && settings[0] && settings[0].siteMapping)
			settings[0].siteMapping.map(item => siteLevel.push({value: item.name, text: item.alias}))

		const keyMetrics = {..._.get(AnalyticsRobin.getResult('engagement_keymetrics'), 'data') }
		const engagement_utilization = {... _.get(AnalyticsRobin.getResult('engagement_utilization'), 'data')}
		const bars = (_.get(AnalyticsRobin.getResult('engagement_breakdown'), 'data') || []).map( o => ({...o}))
		const utilization_breakdown = (_.get(AnalyticsRobin.getResult('utilization_breakdown'), 'data') || []).map( o => ({...o}))
		const sessions_breakdown = (_.get(AnalyticsRobin.getResult('sessions_breakdown'), 'data') || []).map( o => ({...o}))

		if (displayRelativeValues) {
			[keyMetrics, engagement_utilization, bars, utilization_breakdown, sessions_breakdown].forEach( o => {
				const os = Array.isArray(o) ? o : [o]
				os.forEach( e => {
					_.keys(e).filter( k => /relative$/.test(k)).map( k => k.replace(/relative$/, '')).forEach( k => {
						e[k] = e[`${k}relative`] * 100
					})
				})
			})
		}
		const benchmarkMapping = {
			'Previous range': 'Previous period',
			'industry': 'Sector',
			'SegmentSize': 'Size'
		}
		const numberFormaterConditional = displayRelativeValues ? percentage : numberFormater
		const compareBenchmark = this.state.compareBenchmark && !displayRelativeValues
		const engagementDashboard = (
			<DashboardContainer style={{backgroundColor: '#F9FBFF', padding: '1rem', position: 'relative'}}>
				{(clientSettings.isLoading(clientSettings.ACTIONS.FIND_ONE)||clientSettings.isError(clientSettings.ACTIONS.FIND_ONE)) ? <Loader error={clientSettings.isError(clientSettings.ACTIONS.FIND_ONE)}/> : null}
				<div className='dashboard-top-bar'>
					<div style={{display: 'flex', padding: '1rem'}}>
						<DashboardFilter
							width={130}
							type='single'
							title='SITE'
							defaultValue={site}
							style={{marginRight: '1rem'}}
							options={[{value: 'All', text: 'All sites'}, ...siteLevel]}
							onChange={(selected) => this.pageRedirect({site: selected}, true)
						}/>
						<DateRangeFilter value={dateRange} customOption financialStartYear={moment(this.state.filter)} label='DATE RANGE' style={{minWidth: 130, marginRight: '1rem'}}
							onChange={(selected) => this.pageRedirect({
								dateRange: selected.value,
								startDate: moment(selected.startDate).toISOString(),
								endDate: moment(selected.endDate).toISOString()
							}, true)
						}/>
						<DashboardFilter
							width={150}
							type='single'
							text='Compared to'
							title='BENCHMARK'
							defaultValue={benchmark}
							options={[{value: 'Previous range', text: 'Previous period'}, {value: 'industry', text: 'Sector'}, {value: 'SegmentSize', text: 'Size'}]}
							onChange={(selected) => this.pageRedirect({benchmark: selected}, true)
							}/>
						<DashboardFilter
							type='switch'
							text='Display Benchmark'
							checked={compareBenchmark}
							onChange={(selected) => this.setState({compareBenchmark: selected})
							}/>
						<DashboardFilter
							type='switch'
							text='Absolute'
							checked={comparison !== 'absolute'}
							onChange={(selected) => this.pageRedirect({comparison: selected ? 'relative' : 'absolute'}, true)
							}/>
						<span style={{alignSelf: 'flex-end', marginLeft: '0.5rem', marginBottom: '0.8rem'}}>Relative</span>
					</div>
				</div>
				<Dashboard
				structure={{
					items : [1, 3, 4, 4],
					size : 4
				}}>
				<DashboardSection
				section={{
					title : 'Engagement Adjusted',
					tooltip : 'This is the corrected engagement, accounting for duplicate individuals. The formula is `Engagement Adjusted` = `Engagement` * `0.9`'
				}}>
					<DashboardTab>
						<KpiChart
							kpis={[{
								loading: AnalyticsRobin.isLoading('engagement_utilization'),
								error: AnalyticsRobin.isError('engagement_utilization'),
								data : percentage(engagement_utilization.utilization * (displayRelativeValues ? 1 : 100)),
								footer: compareBenchmark ? `${relative(engagement_utilization.utilization, engagement_utilization.utilizationbenchmark)} Vs Benchmark (${percentage(engagement_utilization.utilizationbenchmark * 100)})` : ''
							}]}
							/>
					</DashboardTab>
				</DashboardSection>
				<DashboardSection
				section={{
					title : 'Key usage metrics',
					tooltip : 'Top line engagement tracking metrics'
				}}>
					<DashboardTab>
						<KpiChart
							config={{}}
							kpis={[
								{
									loading: AnalyticsRobin.isLoading('engagement_keymetrics'),
									error: AnalyticsRobin.isError('engagement_keymetrics'),
									title : 'Individual cases',
									data : numberFormaterConditional(keyMetrics.individualcases),
									footer: compareBenchmark ? `${relative(keyMetrics.individualcases, keyMetrics.individualcasesbenchmark)} Vs Benchmark (${numberFormater(keyMetrics.individualcasesbenchmark)})` : ''
								},
								{
									loading: AnalyticsRobin.isLoading('engagement_keymetrics'),
									error: AnalyticsRobin.isError('engagement_keymetrics'),
									title : 'Group Participants',
									data : numberFormaterConditional(keyMetrics.groupparticipants	),
									footer : compareBenchmark ? `${relative(keyMetrics.groupparticipants	, keyMetrics.groupparticipantsbenchmark)} Vs Benchmark (${numberFormater(keyMetrics.groupparticipantsbenchmark)})` : ''
								},
								{
									loading: AnalyticsRobin.isLoading('engagement_keymetrics'),
									error: AnalyticsRobin.isError('engagement_keymetrics'),
									title : 'Online Access',
									data : numberFormaterConditional(keyMetrics.onlineaccess),
									footer : compareBenchmark ? `${relative(keyMetrics.onlineaccess, keyMetrics.onlineaccessbenchmark)} Vs Benchmark (${numberFormater(keyMetrics.onlineaccessbenchmark)})` : ''
								}
						]}
						/>
					</DashboardTab>
				</DashboardSection>
				<DashboardSection
				defaultActiveTab={tab}
				onTabChange={(selected) => this.pageRedirect({tab: selected})}
				section={{
					tabTitles : ['Utilisation', 'Individual Cases', 'Group Participants', 'Online Access', 'Sessions'],
					tabTooltips: [
						'Utilisation Adjusted = Utilisation * 0.9',
						'Number of individuals who attended Face-to-Face',
						'Number of individuals who attended Face-to-Face sessions or Group events as a group',
						'Number of individuals who used online platforms to engagement Icas',
						'Number of Face-to-Face sessions'
					],
					title : 'Key metrics breakdown',
					tooltip : 'Break down of top line engagement metrics'
				}}>
					<DashboardTab>
				<div style={{display: 'flex'}}>
					<DashboardFilter
						width={120}
						type='single'
						text='Split by'
						defaultValue={utilizationbreakdown}
						options={[
							{value: 'product', text: 'Product'},
							{value: 'service', text: 'Service'}
						]}
						onChange={(selected) => this.pageRedirect({utilizationbreakdown: selected, tab: '0/.0'}, true)
					}/>
				</div>
					<BarChart
						loading={AnalyticsRobin.isLoading('utilization_breakdown')}
						error={AnalyticsRobin.isError('utilization_breakdown')}
						bars={compareBenchmark ? ['Actual', `${benchmarkMapping[benchmark]} benchmark`] : ['Actual']}
						config={{showLegend: compareBenchmark}}
						data={utilization_breakdown.sort((a, b) => b.utilization - a.utilization).map( o => {
							return {
								x: o.key || 'Uncategorised',
								'Actual': o.utilization * (displayRelativeValues ? 1 : 100),
								'Previous period benchmark':  o.utilizationbenchmark * 100,
								'Sector benchmark': o.utilizationbenchmark * 100,
								'Size benchmark': o.utilizationbenchmark * 100
							}
						})}
						yaxisUnits={'%'}
						/>
				</DashboardTab>
					<DashboardTab>
					<div style={{display: 'flex'}}>
						<DashboardFilter
							width={140}
							type='single'
							text='Split by'
							defaultValue={keymetrics}
							options={[
								{value: 'service', text: 'Service'},
								{value: 'products', text: 'Products'},
								{value: 'employement', text: 'Employment'},
								{value: 'problem', text: 'Problem'},
								{value: 'problemcluster', text: 'Problem Cluster'},
								{value: 'protocol', text: 'Protocol'},
								{value: 'origin', text: 'Origin'},
								{value: 'severity', text: 'Severity'}]}
								onChange={(selected) => this.pageRedirect({keymetrics: selected, tab: '1/.1'}, true)
							}/>
					</div>
					<BarChart
							loading={AnalyticsRobin.isLoading('engagement_breakdown')}
							error={AnalyticsRobin.isError('engagement_breakdown')}
							bars={compareBenchmark ? ['Actual', `${benchmarkMapping[benchmark]} benchmark`] : ['Actual']}
							config={{showLegend: compareBenchmark}}
							data={bars.sort((a, b) => b.individualcases - a.individualcases).map( o => {
								return {
									x: o.key || 'Uncategorised',
									'Actual': o.individualcases,
									'Previous period benchmark':  o.individualcasesbenchmark,
									'Sector benchmark': o.individualcasesbenchmark,
									'Size benchmark': o.individualcasesbenchmark
								}
							})}
							hideEmptyBars
							relativeToTotal
							/>
					</DashboardTab>
					<DashboardTab>
					<div style={{display: 'flex'}}>
						<DashboardFilter
							width={140}	
							type='single'
							text='Split by'
							defaultValue={keymetrics}
							options={[
								{value: 'service', text: 'Service'},
								{value: 'products', text: 'Products'},
								{value: 'employement', text: 'Employment'},
								{value: 'problem', text: 'Problem'},
								{value: 'problemcluster', text: 'Problem Cluster'},
								{value: 'protocol', text: 'Protocol'},
								{value: 'origin', text: 'Origin'},
								{value: 'severity', text: 'Severity'}]}
								onChange={(selected) => this.pageRedirect({keymetrics: selected, tab: '2/.2'}, true)
							}/>
					</div>
					<BarChart
							loading={AnalyticsRobin.isLoading('engagement_breakdown')}
							error={AnalyticsRobin.isError('engagement_breakdown')}
							bars={compareBenchmark ? ['Actual', `${benchmarkMapping[benchmark]} benchmark`] : ['Actual']}
							config={{showLegend: compareBenchmark}}
							data={bars.sort((a, b) => b.groupparticipants - a.groupparticipants).map( o => {
								return {
									x: o.key || 'Uncategorised',
									'Actual': o.groupparticipants,
									'Previous period benchmark':  o.groupparticipantsbenchmark,
									'Sector benchmark': o.groupparticipantsbenchmark,
									'Size benchmark': o.groupparticipantsbenchmark
								}
							})}
							hideEmptyBars
							relativeToTotal
							/>
					</DashboardTab>
					<DashboardTab>
					<div style={{display: 'flex'}}>
					<DashboardFilter
						width={120}
						type='single'
						text='Split by'
						defaultValue={utilizationbreakdown}
						options={[
							{value: 'product', text: 'Product'},
							{value: 'service', text: 'Service'}
						]}
						onChange={(selected) => this.pageRedirect({utilizationbreakdown: selected, tab: '3/.3'}, true)
						}/>
					</div>
						<BarChart
							loading={AnalyticsRobin.isLoading('engagement_breakdown')}
							error={AnalyticsRobin.isError('engagement_breakdown')}
							config={{showLegend: compareBenchmark}}
							bars={compareBenchmark ? ['Actual', `${benchmarkMapping[benchmark]} benchmark`] : ['Actual']}
							data={bars.sort((a, b) => b.onlineaccess - a.onlineaccess).map( o => {
								return {
									x: o.key || 'Uncategorised',
									'Actual': o.onlineaccess,
									'Previous period benchmark':  o.onlineaccessbenchmark,
									'Sector benchmark': o.onlineaccessbenchmark,
									'Size benchmark': o.onlineaccessbenchmark
								}
							})}
							hideEmptyBars
							relativeToTotal
							/>
					</DashboardTab>
					<DashboardTab>
					<div style={{display: 'flex'}}>
					<DashboardFilter
						width={120}
						type='single'
						text='Split by'
						defaultValue={utilizationbreakdown}
						options={[
							{value: 'product', text: 'Product'},
							{value: 'service', text: 'Service'}
						]}
						onChange={(selected) => this.pageRedirect({utilizationbreakdown: selected, tab: '4/.4'}, true)
						}/>
					</div>
						<BarChart
							loading={AnalyticsRobin.isLoading('sessions_breakdown')}
							error={AnalyticsRobin.isError('sessions_breakdown')}
							config={{showLegend: compareBenchmark}}
							bars={compareBenchmark ? ['Actual', `${benchmarkMapping[benchmark]} benchmark`] : ['Actual']}
							data={sessions_breakdown.sort((a, b) => b.sessions - a.sessions).map( o => {
								return {
									x: o.key || 'Uncategorised',
									'Actual': o.sessions,
									'Previous period benchmark':  o.sessionsbenchmark,
									'Sector benchmark': o.sessionsbenchmark,
									'Size benchmark': o.sessionsbenchmark
								}
							})}
							hideEmptyBars
							relativeToTotal
							/>
					</DashboardTab>
					</DashboardSection>
				</Dashboard>
			</DashboardContainer>
		)
		return(
			<ErrorBoundary>
				<Tabs selectedDefault={0} fillContainer className= 'tab' style={{fontWeight: 'bold'}}
				active={(current) => {
					switch (current) {
						case 1:
						this.props.history.push(`/analysis/client-prioritisation/${clientID}/dashboard/health`)
						break;
						case 2:
						this.props.history.push(`/analysis/client-prioritisation/${clientID}/dashboard/effectiveness`)
						break;
						case 3:
						this.props.history.push(`/analysis/client-prioritisation/${clientID}/dashboard/quality`)
						break;
						case 4:
						this.props.history.push(`/analysis/client-prioritisation/${clientID}/dashboard/roi`)
						break;
						case 5:
						this.props.history.push(`/analysis/client-prioritisation/${clientID}/dashboard/cost-to-serve`)
						break;
					}
				}}
				>
					<TabPane label='Engagement' >
						{hasPermission('/view/dashboard/engagement', PermissionsRobin.getResult('own-permissions')) ? engagementDashboard : noPermission}
					</TabPane>
					<TabPane label='Health' disabled={!hasPermission('/view/dashboard/health', PermissionsRobin.getResult('own-permissions'))}/>
					<TabPane label='Effectiveness' disabled={!hasPermission('/view/dashboard/effectiveness', PermissionsRobin.getResult('own-permissions'))}/>
					<TabPane label='Quality' disabled={!hasPermission('/view/dashboard/quality', PermissionsRobin.getResult('own-permissions'))}/>
					<TabPane label='ROI' disabled={!hasPermission('/view/dashboard/roi', PermissionsRobin.getResult('own-permissions'))}/>
					<TabPane label='CostToServe' disabled={!hasPermission('/view/dashboard/cost-to-serve', PermissionsRobin.getResult('own-permissions'))}/>
				</Tabs>
			</ErrorBoundary>
		)
	}

}
export default Engagement;