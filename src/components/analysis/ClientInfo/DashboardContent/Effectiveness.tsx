import * as React from 'react'
// import {DashboardsContainer} from '@simplus/si-dashboard-client'
import {
	DashboardContainer,
	Dashboard,
	DashboardFilter,
	DashboardSection,
	DashboardTab,
	KpiChart,
	BarChart
} from 'src/components/dashboard'
import * as moment from 'moment'
import * as _ from 'lodash'
import {DateRangeFilter, relative, percentage} from './utils'
import * as queryString from 'query-string'
import {connectRobin} from '@simplus/robin-react'
import {Tabs, TabPane, Loader} from '@simplus/siui'
import { RouteComponentProps } from 'react-router-dom';
import {robins} from 'src/robins'
import {notification} from 'antd'
import {hasPermission} from '../../../../utils'
import {ErrorBoundary} from '../../../../utils/ErrorBoundary'
const {AnalyticsRobin, clientSettings, PermissionsRobin} = robins


@connectRobin([AnalyticsRobin, clientSettings, PermissionsRobin])
export class Effectiveness extends React.Component<RouteComponentProps<{id: string}>> {
	state = {filter: moment('2018-01-01'), compareBenchmark:  true}
	unlisten?: () => void = undefined
	componentWillMount(): void {
		clientSettings.when(clientSettings.findOne(this.props.match.params.id)).then(() => {
			const settings = clientSettings.getModel();
			if (settings[0]) {
				const reportingCycle = moment(settings[0].reportingCycle)
				if (!reportingCycle.isSame(this.state.filter, 'd')) {
					this.setState({filter: reportingCycle})
				}
			}
			this.applyFilters()
		}).catch( err => {
			notification.error({type: 'error', message: 'Could not load settings data !', description: 'There was an error during the settings api execution.'})
		})
		this.unlisten = this.props.history.listen((location, action) => {
			if (action === 'POP')
				this.applyFilters()
		});
	}
	applyFilters(): void {
		const benchmark = queryString.parse(location.hash.split('?')[1]).benchmark || 'Previous range'
		const dateRange = queryString.parse(location.hash.split('?')[1]).dateRange || 'quarter'

		const startDate = queryString.parse(location.hash.split('?')[1]).startDate || moment().startOf('quarter').subtract(1, dateRange).toISOString();
		const endDate = queryString.parse(location.hash.split('?')[1]).endDate || moment().startOf('quarter').toISOString();

		const options: any = {
			startdate : startDate,
			enddate : endDate,
			client : this.props.match.params.id,
			dailyworkhours: 8,
			hourlywage: 120,
		}

		if (benchmark === 'Previous range') {
			options.benchmarkstartdate = moment(startDate).subtract(1, dateRange).toISOString()
			options.benchmarkenddate = moment(endDate).subtract(1, dateRange).toISOString()
		} else {
			options.benchmark = benchmark.split(',')
		}
		AnalyticsRobin.when(AnalyticsRobin.post('effectiveness_keymetrics', '/effectiveness/productivity', options)).catch( err => {
			notification.error({type: 'error', message: 'Could not load effectiveness keymetrics data !', description: 'There was an error during the analytics api execution.'})
		})
		AnalyticsRobin.when(AnalyticsRobin.post('effectiveness_productivity', '/effectiveness/productivity', {...options, split : queryString.parse(this.props .location.search).productivity || 'product'})).catch( err => {
			notification.error({type: 'error', message: 'Could not load effectiveness productivity data !', description: 'There was an error during the analytics api execution.'})
		})
		// TODO check why split is not send in request
		AnalyticsRobin.when(AnalyticsRobin.post('effectiveness_closure', '/effectiveness/closure', {...options, split: 'status'})).catch( err => {
			notification.error({type: 'error', message: 'Could not load effectiveness closure data !', description: 'There was an error during the analytics api execution.'})
		})
	}
	componentWillUnmount(): void {
		if (this.unlisten)
			this.unlisten();
	}
	pageRedirect(update: any, refresh: boolean = false): void {
		const state = {
			...this.urlState(),
			...update}
		this.props.history.push(`${this.props.location.pathname}?${queryString.stringify(state)}`)
		if (refresh)
			this.applyFilters()
	}
	urlState(): any {
		return queryString.parse(location.hash.split('?')[1])
	}
	render(): JSX.Element {
		const clientID = this.props.match.params.id
		const noPermission = <h1 className='no-permission'>You do not have permission to view this page</h1>
		// Query Strings
		const { dateRange, site, benchmark, productivity, comparison, tab} = _.defaults(this.urlState(), {
			dateRange : 'quarter',
			site: 'All',
			benchmark: 'Previous range',
			productivity: 'product',
			comparison: 'absolute',
			tab: '0/.0'
		})
		const displayRelativeValues = comparison === 'relative'

		const settings = clientSettings.getModel()
		const siteLevel: Array<{value: string, text: string}> = []
		if (settings && settings[0] && settings[0].siteMapping)
			settings[0].siteMapping.map(item => siteLevel.push({value: item.name, text: item.alias}))
		const kpis = _.get(AnalyticsRobin.getResult('effectiveness_keymetrics'), 'data', {})
		const closure = _.get(AnalyticsRobin.getResult('effectiveness_closure'), 'data' , [])
		const productivity_value = _.get(AnalyticsRobin.getResult('effectiveness_productivity'), 'data', [])
		const compareBenchmark = this.state.compareBenchmark && !displayRelativeValues
		const benchmarkMapping = {
			'Previous range': 'Previous period',
			'industry': 'Sector',
			'SegmentSize': 'Size'
		}
		const effectivenessDashboard = (
			<DashboardContainer style={{backgroundColor: '#F9FBFF', padding: '1rem', position: 'relative'}}>
				{(clientSettings.isLoading(clientSettings.ACTIONS.FIND_ONE)||clientSettings.isError(clientSettings.ACTIONS.FIND_ONE)) ? <Loader error={clientSettings.isError(clientSettings.ACTIONS.FIND_ONE)}/> : null}
				<div className='dashboard-top-bar'>
					<div style={{display: 'flex', padding: '1rem'}}>
						<DashboardFilter
							width={130}
							type='single'
							title='SITE'
							defaultValue={site}
							style={{marginRight: '1rem'}}
							options={[{value: 'All', text: 'All sites'}, ...siteLevel]}
							onChange={(selected) => this.pageRedirect({site: selected}, true)
						}/>
						<DateRangeFilter value={dateRange} customOption hideMonth financialStartYear={moment(this.state.filter)} label='DATE RANGE' style={{minWidth: 130, marginRight: '1rem'}}
							onChange={(selected) => this.pageRedirect({
								dateRange: selected.value,
								startDate: moment(selected.startDate).toISOString(),
								endDate: moment(selected.endDate).toISOString()
							}, true)
						}/>
						<DashboardFilter
							width={150}
							type='single'
							text='Compared to'
							title='BENCHMARK'
							defaultValue={benchmark}
							options={[{value: 'Previous range', text: 'Previous period'}, {value: 'industry', text: 'Sector'}, {value: 'SegmentSize', text: 'Size'}]}
							onChange={(selected) => this.pageRedirect({benchmark: selected}, true)
						}/>
						<DashboardFilter
							type='switch'
							text='Display Benchmark'
							checked={compareBenchmark}
							onChange={(selected) => this.setState({compareBenchmark: selected})
						}/>
					</div>
				</div>
				<Dashboard
					structure={{
						items : [1, 2, 3],
						size : 3
					}}>
					<DashboardSection section={{
						title : 'Productivity savings',
						tooltip : 'Tooltip..'
					}}>
						<DashboardTab>
							<KpiChart
								kpis={[{
									loading: AnalyticsRobin.isLoading('effectiveness_keymetrics'),
									error: AnalyticsRobin.isError('effectiveness_keymetrics'),
									data : percentage(kpis.productivitysavings * 100),
									footer: compareBenchmark ? `${relative(kpis.productivitysavings, kpis.productivitysavingsbenchmark)} Vs Benchmark (${percentage(kpis.productivitysavingsbenchmark * 100)})` : ''
								}]}
							/>
						</DashboardTab>
					</DashboardSection>
					<DashboardSection section={{
						title : 'Case closure split',
						tooltip : 'Tooltip..'
					}}>
						<DashboardTab>
							<BarChart
								horizontal
								config={{showLegend: compareBenchmark}}
								loading={AnalyticsRobin.isLoading('effectiveness_closure')}
								error={AnalyticsRobin.isError('effectiveness_closure')}
								bars={compareBenchmark ? ['Actual', `${benchmarkMapping[benchmark]} benchmark`] : ['Actual']}
								data={closure.sort((a, b) => b.closure - a.closure).map( c => {
									return {
										x: c.key,
										'Actual': c.closure,
										'Previous period benchmark': c.closurebenchmark,
										'Sector benchmark': c.closurebenchmark,
										'Size benchmark': c.closurebenchmark,
									}
								})}
								relativeToTotal
							/>
						</DashboardTab>
					</DashboardSection>
					<DashboardSection
					defaultActiveTab={tab}
					onTabChange={(selected) => this.pageRedirect({tab: selected})}
					section={{
						tabTitles : ['Productivity	 breakdown', 'Effectiveness word cloud', 'Signifiance movements'],
						title : 'Productivity breakdown',
						tooltip : 'Tooltip..'
					}}>
						<DashboardTab>
						<div style={{display: 'flex'}} className='effectiveness-filter'>
							<DashboardFilter
								width={150}
								type='single'
								text='Split by'
								defaultValue={productivity}
								options={[
									{value: 'service', text: 'Service'},
									{value: 'product', text: 'Product'},
									{value: 'employement', text: 'Employment'},
									{value: 'problem', text: 'Problem'},
									{value: 'problemcluster', text: 'Problem Cluster'},
									{value: 'protocol', text: 'Protocol'},
									{value: 'origin', text: 'Origin'},
									{value: 'severity', text: 'Severity'}]}
								onChange={(selected) => this.pageRedirect({productivity: selected, tab: '0/.0'}, true)
								}/>
						</div>
							<BarChart
								bars={compareBenchmark ? ['Actual', `${benchmarkMapping[benchmark]} benchmark`] : ['Actual']}
								loading={AnalyticsRobin.isLoading('effectiveness_productivity')}
								error={AnalyticsRobin.isError('effectiveness_productivity')}
								config={{showLegend: compareBenchmark}}
								data={productivity_value.sort((a, b) => b.productivitysavings - a.productivitysavings).map( c => {
									return {
										x: c.key,
										'Actual': c.productivitysavings,
										'Previous period benchmark': c.productivitysavingsbenchmark,
										'Sector benchmark': c.productivitysavingsbenchmark,
										'Size benchmark': c.productivitysavingsbenchmark,
									}
								})}
							/>
						</DashboardTab>
					</DashboardSection>
				</Dashboard>
			</DashboardContainer>
		)
		return(
			<ErrorBoundary>
				<Tabs selectedDefault={2} fillContainer className= 'tab' style={{fontWeight: 'bold'}}
				active={(current) => {
					switch (current) {
						case 0:
							this.props.history.push(`/analysis/client-prioritisation/${clientID}/dashboard/engagement`)
							break;
						case 1:
							this.props.history.push(`/analysis/client-prioritisation/${clientID}/dashboard/health`)
							break;
						case 3:
							this.props.history.push(`/analysis/client-prioritisation/${clientID}/dashboard/quality`)
							break;
						case 4:
							this.props.history.push(`/analysis/client-prioritisation/${clientID}/dashboard/roi`)
							break;
						case 5:
							this.props.history.push(`/analysis/client-prioritisation/${clientID}/dashboard/cost-to-serve`)
							break;
					}
				}}
				>
					<TabPane label='Engagement' disabled={!hasPermission('/view/dashboard/engagement', PermissionsRobin.getResult('own-permissions')) }/>
					<TabPane label='Health' disabled={!hasPermission('/view/dashboard/health', PermissionsRobin.getResult('own-permissions'))}/>
					<TabPane label='Effectiveness'>
						{hasPermission('/view/dashboard/effectiveness', PermissionsRobin.getResult('own-permissions')) ? effectivenessDashboard : noPermission}
					</TabPane>
					<TabPane label='Quality' disabled={!hasPermission('/view/dashboard/quality', PermissionsRobin.getResult('own-permissions'))}/>
					<TabPane label='ROI' disabled={!hasPermission('/view/dashboard/roi', PermissionsRobin.getResult('own-permissions'))}/>
					<TabPane label='CostToServe' disabled={!hasPermission('/view/dashboard/cost-to-serve', PermissionsRobin.getResult('own-permissions'))}/>
				</Tabs>
			</ErrorBoundary>
		)
	}

}
export default Effectiveness;