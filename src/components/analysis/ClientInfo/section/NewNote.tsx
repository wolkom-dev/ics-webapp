import * as React from 'react'
import {NotesModel} from '../../../../models'
import * as _ from 'lodash'
import { Modal, Form, Input } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import {ErrorBoundary} from '../../../../utils/ErrorBoundary'
const FormItem = Form.Item;

interface FormProps extends FormComponentProps {
	visible: boolean,
	onCancel(): void,
	onCreate(): void,
	wrappedComponentRef?(formref: any): void
}

export const NewNote = Form.create<NotesModel>({
	mapPropsToFields : props => {
		return _.mapValues(props, (v) => {
			return Form.createFormField({value : v})
		})
	}
})(
	class extends React.Component<FormProps> {
		render(): JSX.Element {
		const { visible, onCancel, onCreate, form } = this.props;
		const { getFieldDecorator, getFieldValue } = form;
		const key = getFieldValue('_key')
		return (
			<ErrorBoundary>
				<Modal
					visible={visible}
					title={key ? 'Edit note' : 'Create a new note'}
					okText={key ? 'Update' : 'Create'}
					onCancel={onCancel}
					onOk={onCreate}
				>
					<Form layout='vertical'>
					<Form.Item
						style={{display: 'none'}}
					>
						{getFieldDecorator('_key', {
						})
						(
							<span hidden />
						)}
					</Form.Item>
					<FormItem label='Title'>
						{getFieldDecorator('title', {
						rules: [{ required: true, message: 'Please input the title of Note!' }],
						})(
						<Input />
						)}
					</FormItem>
					<FormItem label='Description'  className='form_last-form-item'>
						{getFieldDecorator('text')(<Input type='textarea'/>)}
					</FormItem>
					</Form>
				</Modal>
			</ErrorBoundary>
			);
		}
	}
);