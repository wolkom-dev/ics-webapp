import * as React from 'react'
import * as moment from 'moment'
import {Card, Button, Select, ProfilePicture, DatePicker, Loader} from '@simplus/siui'
import {Divider, notification, Icon, Popconfirm} from 'antd'
import {connectRobin} from '@simplus/robin-react'
import {robins} from '../../../robins'
import * as queryString from 'query-string'
import * as _ from 'lodash'
import {hasPermission} from '../../../utils'
import {ErrorBoundary} from '../../../utils/ErrorBoundary'
import { Redirect, RouteComponentProps  } from 'react-router-dom';
import {NewStory} from './section'
const Option = Select.Option;

const {UsersRobin, AuthRobin, StoriesRobin, PermissionsRobin} = robins;

@connectRobin([UsersRobin, AuthRobin, StoriesRobin, PermissionsRobin])
export class Stories extends React.Component<RouteComponentProps<{id: string}>> {
	state = {redirect: false, visible: false, redirectPath: '', story: {}, edit: false, filter: moment('2018-01-01'), loading: false}
	formRef: any = null
	user = AuthRobin.getUserInfo()
	showModal = () => {
		this.setState({ story: {}, visible: true });
	}
	handleCancel = () => {
		this.setState({ visible: false });
	}
	createNotification = (type, exists) => {
		notification[type]({
			message: 'Success !',
			description: exists ? 'Story successfully updated' : 'New story successfully created',
		});
	}
	handleCreate = () => {
		const form = this.formRef.props.form;
		form.validateFields((err, values) => {
			if (err) {
				return;
			}
			if (!err) {
				this.setState({ loading: true, visible: false });
				const id = values[`_key`];
				if (id) {
					let final_values = {...values, id , date: moment().toISOString()}
					final_values = _.omit(final_values, 'id' , '_key')
					StoriesRobin.when(StoriesRobin.update(id, { $set : _.keys(final_values).map( k => ({ [k]: final_values[k] })) } as any)).then(() => { this.setState({ loading: false }); this.applyFilters()})
				} else {
					const final_values = {...values, user: this.user._id, client: this.props.match.params.id, date: moment().toISOString()}
					StoriesRobin.when(StoriesRobin.create(final_values)).then(() => { this.setState({ loading: false }); this.applyFilters()})
				}
				this.createNotification('success', id)
				form.resetFields();
			}
		});
	}
	handleDelete = (key: string) => {
		this.setState({loading: true})
		StoriesRobin.when(StoriesRobin.delete(key, `/${key}`)).then(() => { this.setState({ loading: false }); this.applyFilters()})
	}
	saveFormRef = (formRef) => {
		this.formRef = formRef;
	}
	unlisten?: () => void = undefined
	componentWillMount(): void {
		UsersRobin.find()
		this.applyFilters()
		this.unlisten = this.props.history.listen((location, action) => {
			if (action === 'POP')
				this.applyFilters()
		});
	}
	applyFilters(): void {
		const dateRange = queryString.parse(this.props.location.search).dateRange || 'past 30 days'
		const user = queryString.parse(this.props.location.search).writer || 'All'
		const client = this.props.match.params.id


		let delta = 'month'
		if (dateRange === 'past 90 days')
			delta = 'quarter'
		else if (dateRange === 'past year')
			delta = 'year'
		const startdate = queryString.parse(this.props.location.search).startDate || moment().subtract(1 as any, delta).toISOString();
		const enddate = queryString.parse(this.props.location.search).endDate || moment().toISOString();

		const options = user === 'All' ? {startdate, enddate, client} : {startdate, enddate, client, user}
		StoriesRobin.find({...options})

	}
	componentWillUnmount(): void {
		if (this.unlisten)
			this.unlisten();
	}
	render(): JSX.Element {
		const clientID = this.props.match.params.id
		const baseURI = `/analysis/client-prioritisation/${clientID}/stories`;
		const dateRange = queryString.parse(this.props.location.search).dateRange || 'past 30 days';
		const startDate = queryString.parse(this.props.location.search).startDate || moment().subtract(1, 'month').toISOString();
		const endDate = queryString.parse(this.props.location.search).endDate || moment().toISOString();
		const writer = queryString.parse(this.props.location.search).writer || 'All';
		const users = UsersRobin.getCollection()
		const stories_data = StoriesRobin.getCollection()
		if (this.state.redirect) {
			this.state.redirect = false;
			return <Redirect to={this.state.redirectPath}/>
		}
		return(
			<ErrorBoundary>
				<div className='stories'>
					<div className='stories-top-bar'>
						<h1 className='analysis-tab-title'>Stories</h1>
						{hasPermission('/view/stories/add-story', PermissionsRobin.getResult('own-permissions')) ?
						<Button className='new-story' onClick={this.showModal}>New Story</Button>
						: null}
					</div>
					<NewStory
						{...this.state.story}
						wrappedComponentRef={this.saveFormRef}
						visible={this.state.visible}
						onCancel={this.handleCancel}
						onCreate={this.handleCreate}/>
					<Divider/>
					<div className='stories-filters'>
						<DatePicker
							value={dateRange}
							customOption
							label='DATE RANGE'
							style={{minWidth: 130, marginRight: '1rem'}}
							onChange={(selected) => this.setState(
								{
									redirect: true,
									redirectPath: `${baseURI}?
									dateRange=${selected.value}&
									startDate=${moment(selected.startDate).toISOString()}&
									endDate=${moment(selected.endDate).toISOString()}&
									writer=${writer}`
								})
							}>
							<Option value = 'past 30 days'>Past Month</Option>
							<Option value = 'past 90 days'>Past Quarter</Option>
							<Option value = 'past year'>Past Year</Option>
						</DatePicker>
						<Select
							value={writer}
							label='WRITTEN BY'
							showSearch
							style={{ minWidth: 150 }}
							optionFilterProp='children'
							filterOption={(input, option) => option.props.children!.toString().toLowerCase().indexOf(input.toLowerCase()) >= 0}
							onChange={(selected) => this.setState(
								{
									redirect: true,
									redirectPath: `${baseURI}?
										dateRange=${dateRange}&
										startDate=${startDate}&
										endDate=${endDate}&
										writer=${selected}`
								})
							}>
							<Option value='All'>All users</Option>
							{users.map(user => <Option key={user._id} value={user._id}>{user.name}</Option>)}
						</Select>
					</div>
					<div style={{position: 'relative'}}>
						{(StoriesRobin.isLoading(StoriesRobin.ACTIONS.FIND) || StoriesRobin.isError(StoriesRobin.ACTIONS.FIND) || this.state.loading) ? <Loader error={StoriesRobin.isError(StoriesRobin.ACTIONS.FIND)}/> : null}
						<div className='stories-content'>
						{
							stories_data.map(story => {
								const user_data = users.find(item => item._id === story.user) || {picture: '/src/assets/img/sample-profile.png', name: ''}
								return <Card className='stories-card' key={story._key}>
									<div style={{display: 'flex', justifyContent: 'space-between'}}>
										<div className='stories-title'>{story.title}</div>
										{(story.user === this.user._id) ? <div>
											{hasPermission('/view/stories/edit-story', PermissionsRobin.getResult('own-permissions')) ?
												<Icon
													style={{fontSize: '2rem', marginRight: '1rem', color: '#0090cd', cursor: 'pointer'}}
													type='edit'
													onClick= {() => this.setState({story, visible: true})}
												/> : null
											}
											{hasPermission('/view/stories/delete-story', PermissionsRobin.getResult('own-permissions')) ?
											<Popconfirm title='Are you sure you want to delete this note?' onConfirm={() => this.handleDelete(story._key)} okText='Yes' cancelText='No'>
												<Icon
													style={{fontSize: '2rem', color: '#BB4C49', cursor: 'pointer'}}
													type='close-circle-o'
												/>
											</Popconfirm> : null
											}
										</div>
										: null}
									</div>
									<Divider />
									<div className='stories-text'>{story.text}</div>
									<Divider />
									<div className='stories-footer'>
										<ProfilePicture url={user_data.picture} size={50} rounded/>
										<div className='stories-user-info'>
											<div className='stories-user-name'>{user_data.name}</div>
											<div className='stories-date'>on {moment(story.date).format('MMMM-DD-YYYY')}</div>
										</div>
									</div>
								</Card>
							})
						}
					</div>
					</div>
				</div>
			</ErrorBoundary>
		)
	}

}
export default Stories;