import * as React from 'react'
import * as moment from 'moment'
import {Card, Button, Select, ProfilePicture, DatePicker, Loader} from '@simplus/siui'
import {Divider, notification, Icon, Popconfirm} from 'antd'
import {connectRobin} from '@simplus/robin-react'
import {robins} from '../../../robins'
import * as queryString from 'query-string'
import * as _ from 'lodash'
import { Redirect, RouteComponentProps } from 'react-router-dom';
import {NewNote} from './section'
import {hasPermission} from '../../../utils'
import {ErrorBoundary} from '../../../utils/ErrorBoundary'
const Option = Select.Option;
const {UsersRobin, AuthRobin, NotesRobin, PermissionsRobin} = robins;

@connectRobin([UsersRobin, AuthRobin, NotesRobin, PermissionsRobin])
export class Notes extends React.Component<RouteComponentProps<{id: string}>> {
	state = {redirect: false, visible: false, redirectPath: '', notes: {}, edit: false, filter: moment('2018-01-01'), loading: false}
	formRef: any = null
	user = AuthRobin.getUserInfo()
	showModal = () => {
		this.setState({ notes: {}, visible: true});
	}
	handleCancel = () => {
		this.setState({ visible: false });
	}
	createNotification = (type, exists) => {
		notification[type]({
			message: 'Success !',
			description: exists ? 'Note successfully updated' : 'New note successfully created',
		});
	}

	handleCreate = () => {
		const form = this.formRef.props.form;
		form.validateFields((err, values) => {
			if (err) {
				return;
			}
			if (!err) {
					const id = values[`_key`];
					this.setState({ loading: true, visible: false });
					if (id) {
						let final_values = {...values, id , date: moment().toISOString()}
						final_values = _.omit(final_values, 'id' , '_key')
						NotesRobin.when(NotesRobin.update(id, { $set : _.keys(final_values).map( k => ({ [k]: final_values[k] })) } as any)).then(() => { this.setState({ loading: false }); this.applyFilters()})
					} else {
						const final_values = {...values, user: this.user._id, client: this.props.match.params.id, date: moment().toISOString()}
						NotesRobin.when(NotesRobin.create(final_values)).then(() => { this.setState({ loading: false }); this.applyFilters()})
					}
					this.createNotification('success', id)
					form.resetFields();
				}
			});
	}
	handleDelete = (key: string) => {
		this.setState({ loading: true});
		NotesRobin.when(NotesRobin.delete(key, `/${key}`)).then(() => { this.setState({ loading: false }); this.applyFilters()})
	}
	saveFormRef = (formRef) => {
		this.formRef = formRef;
	}
	unlisten?: () => void = undefined
	componentWillMount(): void {
		UsersRobin.find()
		this.applyFilters()
		this.unlisten = this.props.history.listen((location, action) => {
			if (action === 'POP')
				this.applyFilters()
		});
	}
	applyFilters(): void {
		const dateRange = queryString.parse(this.props.location.search).dateRange || 'past 30 days'
		const user = this.user._id
		const client = this.props.match.params.id

		let delta = 'month'
		if (dateRange === 'past 90 days')
			delta = 'quarter'
		else if (dateRange === 'past year')
			delta = 'year'
		const startdate = queryString.parse(this.props.location.search).startDate || moment().subtract(1 as any, delta).toISOString();
		const enddate = queryString.parse(this.props.location.search).endDate || moment().toISOString();

		const options = {startdate, enddate, client, user}
		NotesRobin.find({...options})

	}
	componentWillUnmount(): void {
		if (this.unlisten)
			this.unlisten();
	}
	render(): JSX.Element {
		const clientID = this.props.match.params.id
		const baseURI = `/analysis/client-prioritisation/${clientID}/notes`;
		const dateRange = queryString.parse(this.props.location.search).dateRange || 'past 30 days';
		const writer = queryString.parse(this.props.location.search).writer || 'All';
		const users = UsersRobin.getCollection()
		const notes_data = NotesRobin.getCollection()
		if (this.state.redirect) {
			this.state.redirect = false;
			return <Redirect to={this.state.redirectPath}/>
		}
		return(
			<ErrorBoundary>
				<div className='notes'>
					<div className='notes-top-bar'>
						<h1 className='analysis-tab-title'>Notes</h1>
						{hasPermission('/view/notes/add-note', PermissionsRobin.getResult('own-permissions')) ?
						<Button className='new-story' onClick={this.showModal}>New note</Button>
						: null}
						<NewNote
							{...this.state.notes}
							wrappedComponentRef={this.saveFormRef}
							visible={this.state.visible}
							onCancel={this.handleCancel}
							onCreate={this.handleCreate}/>
					</div>
					<Divider/>
					<div className='notes-filters'>
						<DatePicker
							value={dateRange}
							customOption
							label='DATE RANGE'
							style={{minWidth: 130, marginRight: '1rem'}}
							onChange={(selected) => this.setState(
								{
									redirect: true,
									redirectPath: `${baseURI}?
									dateRange=${selected.value}&
									startDate=${moment(selected.startDate).toISOString()}&
									endDate=${moment(selected.endDate).toISOString()}&
									writer=${writer}`
								})
							}>
							<Option value = 'past 30 days'>Past Month</Option>
							<Option value = 'past 90 days'>Past Quarter</Option>
							<Option value = 'past year'>Past Year</Option>
						</DatePicker>
					</div>
					<div style={{position: 'relative'}}>
						{(NotesRobin.isLoading(NotesRobin.ACTIONS.FIND) || NotesRobin.isError(NotesRobin.ACTIONS.FIND) || this.state.loading) ? <Loader error={NotesRobin.isError(NotesRobin.ACTIONS.FIND)}/> : null}
						<div className='notes-content'>
							{
								notes_data.map(notes => {
									const user_data = users.find(item => item._id === notes.user) || {picture: '/src/assets/img/sample-profile.png', name: ''}
									return <Card className='notes-card' key={notes._key}>
										<div style={{display: 'flex', justifyContent: 'space-between'}}>
											<div className='notes-title'>{notes.title}</div>
											{(notes.user === this.user._id) ? <div>
												{hasPermission('/view/notes/edit-note', PermissionsRobin.getResult('own-permissions')) ?
												<Icon
													style={{fontSize: '2rem', marginRight: '1rem', color: '#0090cd', cursor: 'pointer'}}
													type='edit'
													onClick= {() => this.setState({notes, visible: true})}
												/> : null }
												{hasPermission('/view/notes/delete-note', PermissionsRobin.getResult('own-permissions')) ?
												<Popconfirm title='Are you sure you want to delete this note?' onConfirm={() => this.handleDelete(notes._key)} okText='Yes' cancelText='No'>
													<Icon
														style={{fontSize: '2rem', color: '#BB4C49', cursor: 'pointer'}}
														type='close-circle-o'
													/>
												</Popconfirm> : null }
											</div>
											: null}
										</div>
										<Divider />
										<div className='notes-text'>{notes.text}</div>
										<Divider />
										<div className='notes-footer'>
											<ProfilePicture url={user_data.picture} size={50} rounded/>
											<div className='notes-user-info'>
												<div className='notes-user-name'>{user_data.name}</div>
												<div className='notes-date'>on {moment(notes.date).format('MMMM-DD-YYYY')}</div>
											</div>
										</div>
									</Card>
								})
							}
						</div>
					</div>
				</div>
			</ErrorBoundary>
		)
	}

}
export default Notes;