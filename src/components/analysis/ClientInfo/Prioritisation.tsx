import * as React from 'react'
import * as moment from 'moment'
import {Link} from 'react-router-dom'
import {Select, Loader} from '@simplus/siui'
import {DateRangeFilter} from './DashboardContent/utils'
import {Divider} from 'antd'
import {TableChart} from '../../dashboard'
import * as _ from 'lodash'
import * as queryString from 'query-string'
import { RouteComponentProps } from 'react-router-dom';
import {connectRobin} from '@simplus/robin-react'
import {robins} from '../../../robins'
import {Icon, Modal, notification} from 'antd'
import {ErrorBoundary} from '../../../utils/ErrorBoundary'
import {numberFormater, percentage} from './DashboardContent/utils'
const Option = Select.Option
const {clientSettings, AnalyticsRobin} = robins;

@connectRobin([clientSettings, AnalyticsRobin])
export class Prioritisation extends React.Component<RouteComponentProps<{id: string, department?: string, key?: string}>> {
	state = {redirect: false, redirectPath: '', filter: moment('2018-01-01'), noSettingFlag: false, loading: false}
	unlisten?: () => void = undefined
	componentWillMount(): void {
		this.setState({loading: true})
		clientSettings.when(clientSettings.findOne(this.props.match.params.id)).then(() => {
			const settings = clientSettings.getModel();
			if (settings[0]) {
				const reportingCycle = moment(settings[0].reportingCycle)
				if (!reportingCycle.isSame(this.state.filter, 'd')) {
					this.setState({filter: reportingCycle})
				}
			}
			this.applyFilters()
		}).catch( err => {
			notification.error({type: 'error', message: 'Could not load settings data !', description: 'There was an error during the settings api execution.'})
		})
		this.unlisten = this.props.history.listen((location, action) => {
			if (action === 'POP')
				this.applyFilters()
		});
	}
	drillDown(kpi: string, key: string): void {
		const dateRange = queryString.parse(this.props.location.search).dateRange || 'month'

		const startDate = queryString.parse(this.props.location.search).startDate || moment().startOf('month').subtract(1, dateRange).toISOString();
		const endDate = queryString.parse(this.props.location.search).endDate || moment().startOf('month').toISOString();

		const options: any = {
			startdate : startDate,
			enddate : endDate,
			client : this.props.match.params.id,
			dailyworkhours: 8,
			branch: key,
			benchmarkstartdate: moment(startDate).subtract(1, dateRange).toISOString(),
			benchmarkenddate: moment(endDate).subtract(1, dateRange).toISOString()
		} 
		AnalyticsRobin.when(AnalyticsRobin.post('prioritization_drill_down', `/attribution/${kpi}`,  {...options})).catch( err => {
			notification.error({type: 'error', message: 'Could not load attribution data !', description: 'There was an error during the attribution drilldown api execution.'})
		})
	}
	applyFilters(): void {
		const dateRange = queryString.parse(location.hash.split('?')[1]).dateRange || 'month'

		const startDate = queryString.parse(location.hash.split('?')[1]).startDate || moment().startOf('month').subtract(1, dateRange).toISOString();
		const endDate = queryString.parse(location.hash.split('?')[1]).endDate || moment().startOf('month').toISOString();
		const site = queryString.parse(location.hash.split('?')[1]).site || 'SiteLevel 1'

		const options: any = {
			startdate : startDate,
			enddate : endDate,
			client : this.props.match.params.id,
			dailyworkhours: 8,
			split : 'sitelevel',
			benchmarkstartdate: moment(startDate).subtract(1, dateRange).toISOString(),
			benchmarkenddate: moment(endDate).subtract(1, dateRange).toISOString()
		}
		
		this.setState({loading: false})
		AnalyticsRobin.when(AnalyticsRobin.post('prioritization_table', '/attribution/summary',  {...options, site: site})).catch( err => {
			notification.error({type: 'error', message: 'Could not load attribution summary data !', description: 'There was an error during the attribution summary api execution.'})
		})
	}

	componentWillUnmount(): void {
		if (this.unlisten)
			this.unlisten();
	}

	pageRedirect(update: any, refrsh: boolean = false): void {
		const state = {
			...this.urlState(),
			...update}
		this.props.history.push(`${this.props.location.pathname}?${queryString.stringify(state)}`)
		if (refrsh)
			this.applyFilters()
	}

	urlState(): any {
		return queryString.parse(location.hash.split('?')[1])
	}

	render(): JSX.Element {
		const settings = clientSettings.getModel() || [{siteMapping: []}]
		const siteLevelOptions: any = [];
		if (settings && settings[0] && settings[0].siteMapping)
			settings[0].siteMapping.map(item => siteLevelOptions.push({value: item.name, text: item.alias}))
		const { dateRange, site, startDate, endDate} = _.defaults(this.urlState(), {
			dateRange : 'month',
			startDate : moment().startOf('month').subtract(1, 'month').toISOString(),
			endDate : moment().startOf('month').toISOString(),
			site: (siteLevelOptions[0]||{text: 'Site level 1'}).text
		})
		const result = _.get(AnalyticsRobin.getResult('prioritization_table'), 'data', []);
		const resultDrillDown = _.get(AnalyticsRobin.getResult('prioritization_drill_down'), 'data', []);
		return(
			<ErrorBoundary>
				<div className='proritisation'>
				<Modal
					visible={!!this.props.match.params.key}
					title={`Attribution analysis (${this.props.match.params.department})`}
					footer={null}
					onCancel={() => {
						this.props.history.push(this.props.location.pathname.replace(`/${this.props.match.params.department}/${this.props.match.params.key}`, ''))
					}}
				>
					<TableChart
						loading={AnalyticsRobin.isLoading('prioritization_drill_down')}
						columns={[
							{Header : 'Description', accessor: 'key'},
							{Header : ({engagement : 'Engagement', workimpact : 'Work impact', productivity : 'Productivity'})[this.props.match.params.key as string], accessor: 'current'},
							{Header : 'Percentage Change', accessor: 'delta'},
						]}
						data={resultDrillDown}
					/>
				</Modal>
				<h1 className='analysis-tab-title'>Prioritisation</h1>
				<Divider/>
					<div className='proritisation-filters' style={{position: 'relative'}}>
					{(clientSettings.isLoading(clientSettings.ACTIONS.FIND_ONE)||clientSettings.isError(clientSettings.ACTIONS.FIND_ONE)) ? <Loader error={clientSettings.isError(clientSettings.ACTIONS.FIND_ONE)}/> : null}
						<DateRangeFilter
							value={dateRange}
							customOption
							financialStartYear={moment(this.state.filter)}
							label='DATE RANGE'
							style={{marginRight: '1rem', minWidth: 130}}
							onChange={(selected) => this.pageRedirect({
								dateRange: selected.value,
								startDate: moment(selected.startDate).toISOString(),
								endDate: moment(selected.endDate).toISOString()
							}, true)
							}>
						</DateRangeFilter>
						<Select
							value={site}
							label='SITE'
							onChange={(selected) => this.pageRedirect({site: selected}, true)
							}>
							{siteLevelOptions.map((item, index) => <Option key={index} value={item.value}>{item.text}</Option>)}
						</Select>
					</div>
					<TableChart
						sortable={true}
						// loading={AnalyticsRobin.isLoading('prioritization_table') || this.state.loading}
						loading={AnalyticsRobin.isLoading('prioritization_table')}
						columns={[
							{Header : 'Department', accessor: 'name'},
							{Header : 'Engagement', accessor: 'engagement', Cell: props => {
								return <div style={{textAlign : 'left', display: 'flex'}}> <div style={{width: '4rem', marginLeft: '1rem'}}>{ percentage(props.row.engagement * 100) }</div>
									<Link to={`/analysis/client-prioritisation/${this.props.match.params.id}/dashboard/engagement?dateRange=${encodeURI(dateRange)}&endDate=${encodeURI(moment(endDate).toISOString())}&startDate=${encodeURI(moment(startDate).toISOString())}`}>See dashboard</Link> |
									<Link onClick={() => {
										this.drillDown('engagement', props.row._original.key)
									}} to={`${this.props.location.pathname}/${props.row._original.key}/engagement`}> drill down <Icon type='filter'/></Link></div>
							}},
							{Header : 'Work impact score', accessor: 'workimpact', Cell: props => {
								return <div style={{textAlign : 'left', display: 'flex'}}> <div style={{width: '4rem', marginLeft: '1rem'}}>{ numberFormater(props.row.workimpact)}</div>
									<Link to={`/analysis/client-prioritisation/${this.props.match.params.id}/dashboard/health?dateRange=${encodeURI(dateRange)}&endDate=${encodeURI(moment(endDate).toISOString())}&startDate=${encodeURI(moment(startDate).toISOString())}`}>See dashboard</Link> |
									<Link onClick={() => {
										this.drillDown('workimpact', props.row._original.key)
									}} to={`${this.props.location.pathname}/${props.row._original.key}/workimpact`}> drill down <Icon type='filter'/></Link></div>
								}},
								{Header : 'Productivity savings', accessor: 'productivity', Cell: props => {
									return <div style={{textAlign : 'left', display: 'flex'}}> <div style={{width: '4rem', marginLeft: '1rem'}}>{ percentage(props.row.productivity * 100)}</div>
									<Link to={`/analysis/client-prioritisation/${this.props.match.params.id}/dashboard/effectiveness?dateRange=${encodeURI(dateRange)}&endDate=${encodeURI(moment(endDate).toISOString())}&startDate=${encodeURI(moment(startDate).toISOString())}`}>See dashboard</Link> |
									<Link onClick={() => {
										this.drillDown('productivity', props.row._original.key)
									}} to={`${this.props.location.pathname}/${props.row._original.key}/productivity`}> drill down <Icon type='filter'/></Link></div>
							}},
						]}
						data={
							result || []
						}
					/>
				</div>
			</ErrorBoundary>
		)
	}

}
export default Prioritisation;