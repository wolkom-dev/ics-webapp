import * as React from 'react';
import {HashRouter as Router, Route, Switch, Redirect} from 'react-router-dom'
import {CrossClientLayout} from './CrossClientAnalysis/CrossClientLayout'
import {ClientPrioritisation} from './ClientPrioritisation/ClientPrioritisation'
import {ClientInfo} from './ClientInfo'
import { connectRobin } from '@simplus/robin-react';
import {robins} from '../../robins'
import { hasPermission } from '../../utils';

const {AuthRobin, PermissionsRobin} = robins

@connectRobin([AuthRobin, PermissionsRobin])
export class AnalysisApp extends React.Component {
	/**
	 *  Render method
	 */
	render(): JSX.Element {
		const user = AuthRobin.getUserInfo();
		return(
			<Router>
				<Switch>
					{hasPermission('/view/cross-client-analysis', PermissionsRobin.getResult('own-permissions')) ?
						<Redirect exact from='/analysis' to='/analysis/cross-client-analysis'/>
						: (hasPermission('/view/clients-prioritization', PermissionsRobin.getResult('own-permissions')) ?
							<Redirect exact from='/analysis' to='/analysis/client-prioritisation'/> :
							<Redirect exact from='/analysis' to={`/analysis/client-prioritisation/${user.client}`}/>)
					}
					<Route exact path='/analysis/cross-client-analysis' component={CrossClientLayout} />
					<Route exact path='/analysis/client-prioritisation' component={ClientPrioritisation} />
					<Route path='/analysis/client-prioritisation/:id' component={ClientInfo} />
				</Switch>
			</Router>
		)
	}
}

export default AnalysisApp;