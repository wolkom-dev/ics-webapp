import * as React from 'react'
import {TitledCard, ProfilePicture} from '@simplus/siui'
import {Button, Icon, Input, message, notification, Form} from 'antd'
import { FormComponentProps } from 'antd/lib/form';
import * as UUID from 'uuid';
import {robins} from '../../../../robins'
import {withRouter} from 'react-router-dom'
import axios from 'axios'
import * as _ from 'lodash'
import {connectRobin} from '@simplus/robin-react'
import Dropzone from 'react-dropzone';
import {UsersModel} from '../../../../models'
import * as owasp from 'owasp-password-strength-test'
import { hasPermission } from '../../../../utils';
import {ErrorBoundary} from '../../../../utils/ErrorBoundary'
const {UsersRobin, PermissionsRobin} = robins;
export interface EditProfileOwnProps {
}

export interface EditProfileOwnStates {
	userImg: string,
}
const uploadButton = (
	<div style={{width: 150, height: 150, display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', border: '1px dashed #CCC'}}>
		<Icon type={'plus'} />
		<div className='ant-upload-text'>Upload</div>
	</div>
);

const wr = withRouter as any

@wr
@connectRobin([UsersRobin, PermissionsRobin])
@(Form.create({
	mapPropsToFields: (props) => {
		return  _.omit(_.mapValues({...(UsersRobin.getModel() || {})}, v => {
			return Form.createFormField({ value: v})
		}), ['password'])
	}
}) as any)
export class EditProfile extends React.Component<EditProfileOwnProps&FormComponentProps, EditProfileOwnStates> {

	constructor(props: any) {
		super(props);
		this.state = {
			userImg: '',
		};
	}

	uploadImage = (file: any) => {
		if (file[0].size < (1024)) {
			message.error('Image size is too small');
		} else
		if (file[0].size > (1024 * 1024)) {
			message.error('Image size is too large');
		} else {
			const formData = new FormData()
			formData.append('file', file[0])
			formData.append('public_id', UUID())
			formData.append('upload_preset', 'p4f97hzq')
			axios({
				url : '/v1_1/dfprwegge/upload',
				method : 'POST',
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				},
				baseURL: 'https://api.cloudinary.com',
				data : formData
			}).then( (res) => {
				this.setState({
					userImg: res.data.secure_url,
				});
			}).catch( () => {
				message.error(`Image upload failed`)
			})
		}
	}
	componentDidMount(): void {
		UsersRobin.findOne('own')
	}
	updateFunction = (UserData: UsersModel) => {
		const picture = this.state.userImg || UserData.picture
		const {getFieldValue, validateFields } = this.props.form
		validateFields( err => {
			if (!err) {
				UsersRobin.update('own', {name : getFieldValue('name'), picture } as UsersModel);
				notification.open({
					message: 'Success',
					description: 'User Data Updated Successfully, You will be redirected shortly'
				})
				setTimeout(() => {
					UsersRobin.findOne('own')
				} , 500)
			}
		})
	}
	render(): JSX.Element {
		const { getFieldDecorator, getFieldValue, getFieldError } = this.props.form
		const UserData: UsersModel = UsersRobin.getModel() || {}
		const props_img = UserData.picture
		return(
			<ErrorBoundary>
				<div style={{width: '500px', marginRight: 'auto', marginLeft: 'auto'}}>
				<TitledCard
					margin
					titleStyle={{ background : 'rgb(43, 75, 126)' }}
					rounded
					loading={UserData === null || UserData === undefined}
					picture=
					{
						<Dropzone
							style={{cursor: 'pointer', display: 'flex', justifyContent: 'center'}}
							multiple={false}
							accept='image/*'
							onDrop={this.uploadImage}>
							{((this.state.userImg || props_img)) ? <ProfilePicture outstand url={this.state.userImg || props_img} size={150}/> : uploadButton}
						</Dropzone>
					}>
					<Form onSubmit={evt => {
						evt.preventDefault()
						this.updateFunction(UserData)
					}} style={{width: '300px', marginLeft: 'auto', marginRight: 'auto'}}>
						<h2 style={{textAlign : 'center', paddingBottom: '20px'}}>Edit Profile</h2>
							<div style={{width: '300px', marginRight: 'auto', marginLeft: 'auto'}}>
								<Form.Item>
									{getFieldDecorator('_id')(
										<span hidden />
									)}
								</Form.Item>
								<Form.Item>
									{getFieldDecorator('name', {
										rules: [{required: true, message: 'Enter name'}]
									})(
										<Input key='name' placeholder='Enter User Name' prefix={<Icon type='user' />} style = {{width: '300px'}}/>
									)}
								</Form.Item>
								<Form.Item>
									{getFieldDecorator('email', {
									})(
										<Input autoComplete={'off'} disabled placeholder='Enter User Email' prefix={<Icon type='mail' />} style = {{width: '300px'}}/>
									)}
								</Form.Item>
								<Form.Item>
									{getFieldDecorator('password', {
										rules: [{
											validator : (rule, value, done) => {
												if (!value) {
													return done()
												}
												owasp.config({minLength : 8})
												const res = owasp.test(value)
												if (res.strong) {
													done()
												} else {
													done(res.errors[0])
												}
											}
										}]
									})(
										<Input autoComplete={'off'}  type='password' placeholder='Enter User Password' prefix={<Icon type='key' />} style = {{width: '300px'}} />
									)}
								</Form.Item>
								<Form.Item>
									{getFieldValue('password') ? getFieldDecorator('password-confirm', {
										rules: [
											{
												required: this.props.form.isFieldTouched('password') && !!getFieldValue('password'),
												message: 'Field is required'
											},
											{ validator: (rule, value, done) => {
													if (value !== getFieldValue('password'))
														return done('Passwords need to be the same')
													done()
												}, message: 'Passwords need to be the same' }
									]
									})(
										<span>
											<Input placeholder='Confirm password'  type='password' prefix={<Icon type='key' />} style = {{width: '300px'}} />
											<Button disabled={!!(!this.props.form.isFieldTouched('password') || !this.props.form.isFieldTouched('password-confirm') || getFieldError('password') || getFieldError('password-confirm'))} htmlType='button' onClick={() => {
												UsersRobin.put('update-password', `/own/password`, {
													password: getFieldValue('password')
												})
											}}>Save password</Button>
										</span>
									) : null }
								</Form.Item>
							</div>
						<div style = {{display: 'flex', justifyContent: 'space-evenly'}}>
							<Button disabled={!hasPermission('/view/user/edit-profile', PermissionsRobin.getResult('own-permissions'))} htmlType='submit' >Update Profile</Button>
						</div>
					</Form>
				</TitledCard>
				</div>
			</ErrorBoundary>
		)
	}
}

export default EditProfile