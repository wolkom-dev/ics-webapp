import * as React from 'react';
import axios from 'axios'
import * as UUID from 'uuid';
import {ClientSettingsModel} from '../../models/ClientSettingsModel'
import * as moment from 'moment';
import * as _ from 'lodash'
import { Select, Form, Button, Input, Divider, message, Icon, DatePicker, notification, Col} from 'antd';
import Dropzone from 'react-dropzone';
import {ProfilePicture, Loader} from '@simplus/siui'
import { FormComponentProps } from 'antd/lib/form';
const Option = Select.Option
import {connectRobin} from '@simplus/robin-react'
import {robins} from '../../robins'
import { Redirect } from 'react-router';
import { hasPermission } from '../../utils';
import {ErrorBoundary} from '../../utils/ErrorBoundary'

const {clientSettings, PermissionsRobin} = robins;

interface State {
	updated: boolean,
	loadingImage: boolean,
	loading: boolean,
	clientImg: string,
}
@connectRobin([clientSettings, PermissionsRobin])
class ClientSettingsForm extends React.Component<FormComponentProps, State> {
	constructor(props: FormComponentProps) {
		super(props);
		const {getFieldValue } = this.props.form;
		this.state = {
			updated: false,
			loadingImage: false,
			loading: false,
			clientImg: getFieldValue('img')
		};
	}

	uploadImage = (file: any) => {
		if (file[0].size < (1024)) {
			message.error('Image size is too small');
		} else
		if (file[0].size > (1024 * 1024)) {
			message.error('Image size is too large');
		} else {
			const formData = new FormData()
			formData.append('file', file[0])
			formData.append('public_id', UUID())
			formData.append('upload_preset', 'p4f97hzq')
			this.setState({loadingImage: true})
			axios({
				url : '/v1_1/dfprwegge/upload',
				method : 'POST',
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				},
				baseURL: 'https://api.cloudinary.com',
				data : formData
			}).then( (res) => {
				this.setState({
					clientImg: res.data.secure_url,
					loadingImage: false
				});

			}).catch( () => {
				this.setState({
					loadingImage: false
				});
				message.error(`Image upload failed`)
			})
		}
	}

	handleSubmit = (e: React.FormEvent<HTMLInputElement>) => {
		e.preventDefault();
		this.setState({loading: true})
		this.props.form.validateFieldsAndScroll((err: Error, fieldsValue: any) => {
		if (!err) {
			const id = fieldsValue[`_key`];
			const { upload, callbacks, callAnswerRate, sessions, SLM1_Name, SLM1_Alias, SLM2_Name, SLM2_Alias, SLM3_Name, SLM3_Alias, SLM4_Name, SLM4_Alias, SLM5_Name, SLM5_Alias, SLM6_Name, SLM6_Alias, ...values} = fieldsValue;
			const sla = {
				callbacks: callbacks,
				callAnswerRate: callAnswerRate,
				sessions: sessions
			}
			const siteMapping: any = [];
			if (SLM1_Name)
				siteMapping.push({
					name: SLM1_Name,
					alias: SLM1_Alias
				})
			if (SLM2_Name)
			siteMapping.push({
				name: SLM2_Name,
				alias: SLM2_Alias
			})
			if (SLM3_Name)
			siteMapping.push({
				name: SLM3_Name,
				alias: SLM3_Alias
			})
			if (SLM4_Name)
			siteMapping.push({
				name: SLM4_Name,
				alias: SLM4_Alias
			})
			if (SLM5_Name)
			siteMapping.push({
				name: SLM5_Name,
				alias: SLM5_Alias
			})
			if (SLM6_Name)
			siteMapping.push({
				name: SLM6_Name,
				alias: SLM6_Alias
			})
			const final_values = {
				...values,
				img: this.state.clientImg || fieldsValue[`img`],
				sla: sla,
				siteMapping: siteMapping
			}
			if (id) {
				clientSettings.when(clientSettings.update(id, final_values)).then(() => 
				{ 
					clientSettings.find({});
					this.setState({updated: true})}
				)
			} else {
				clientSettings.when(clientSettings.create(final_values)).then(() => 
				{ 
					clientSettings.find({});
					this.setState({updated: true})}
				)
			}
		}
		});
	}


	/**
	 *  Render method
	 */
	render(): JSX.Element {
		if (this.state.updated) {
				notification[`success`]({
					message: 'Settings updated',
					description: '',
				});
				return <Redirect to='/settings' />
		}
		const { getFieldDecorator, getFieldValue } = this.props.form;
		const props_img = getFieldValue('img')
		// const client_id = getFieldValue('client')
		const SLM1 = getFieldValue('SLM1_Name')
		const SLM2 = getFieldValue('SLM2_Name')
		const SLM3 = getFieldValue('SLM3_Name')
		const SLM4 = getFieldValue('SLM4_Name')
		const SLM5 = getFieldValue('SLM5_Name')
		const SLM6 = getFieldValue('SLM6_Name')
		const users = getFieldValue('users')
		const productsList = getFieldValue('productsList')
		const formItemLayout = {
			labelCol: {
				xs: { span: 24 },
				sm: { span: 10 },
			},
			wrapperCol: {
				xs: { span: 24 },
				sm: { span: 6 },
			},
		};
		const multipleItemsLayout = {
			labelCol: {
				xs: { span: 24 },
				sm: { span: 10 },
			},
			wrapperCol: {
				xs: { span: 24 },
				sm: { span: 12 },
			},
		};
		const tailFormItemLayout = {
			wrapperCol: {
				xs: {
					span: 24,
					offset: 0,
				},
				sm: {
					span: 16,
					offset: 10,
				},
			},
		};
		const Dates: JSX.Element[] = []
		for (let i = 1; i <= 31; i++) {
			Dates.push(<Option key={i} value={i.toString()}> {i} </Option>)
		}

		const uploadButton = (
			<div style={{width: 150, height: 150, display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', border: '1px dashed #CCC'}}>
				<Icon type={this.state.loadingImage ? 'loading' : 'plus'} />
				<div className='ant-upload-text'>Upload</div>
			</div>
		);

		return <ErrorBoundary>
			<div style={{position: 'relative'}}>
				{this.state.loading ? <Loader /> : null}
				<Form style={{marginTop: '1rem'}} onSubmit={this.handleSubmit}>
					<Divider>Client Information</Divider>
					<Form.Item
						style={{display: 'none'}}
					>
						{getFieldDecorator('_key', {
						})
						(
							<span hidden />
						)}
					</Form.Item>
					<Form.Item
						style={{display: 'none'}}
						label='Client ID'
					>
						{getFieldDecorator('client', {
						})
						(
							<span hidden />
						)}
					</Form.Item>
					<Col span={24}>
					<Form.Item
					{...formItemLayout}
					label='Client Logo'
					>
					{getFieldDecorator('upload', {
					})(
						<Dropzone
								style={{cursor: 'pointer'}}
								multiple={false}
								accept='image/*'
								onDrop={this.uploadImage}>

							{((this.state.clientImg || props_img) && !this.state.loadingImage) ? <ProfilePicture outstand url={this.state.clientImg || props_img} size={150}/> : uploadButton}
						</Dropzone>
					)}
					</Form.Item>
					</Col>
					<Col span={12}>
					<Form.Item
						{...multipleItemsLayout}
						label='ICAS custodian'
					>
						{getFieldDecorator('icasCustodian', {})
						(
							<Select placeholder='Select a ICAS user'
								showSearch
								style={{ minWidth: 150 }}
								optionFilterProp='children'
								filterOption={(input, option) => option.props.children!.toString().toLowerCase().indexOf(input.toLowerCase()) >= 0}
							>
								{users.map(user => <Option key={user._id}>{user.name}</Option>)}
							</Select>
						)}
					</Form.Item>
					</Col>
					<Col span={12}>
					<Form.Item
						{...multipleItemsLayout}
						label='Client custodian'
					>
						{getFieldDecorator('clientCustodian', {})
						(
							<Select  placeholder='Select a client user'
								showSearch
								style={{ minWidth: 150 }}
								optionFilterProp='children'
								filterOption={(input, option) => option.props.children!.toString().toLowerCase().indexOf(input.toLowerCase()) >= 0}
							>
								{users.map(user => <Option key={user._id}>{user.name}</Option>)}
							</Select>
						)}
					</Form.Item>
					</Col>
					<Divider>Graph data parameters</Divider>
					<Col span={12}>
					<Form.Item
						{...multipleItemsLayout}
						label='Working hours per Day'
					>
						{getFieldDecorator('avgWorkingHours', {
							initialValue: 8
						})
						(
							<Input type='number'/>
						)}
					</Form.Item>
					</Col>
					<Col span={12}>
					<Form.Item
						{...multipleItemsLayout}
						label='Average employee wage'
					>
						{getFieldDecorator('avgEmployeeWage', {
							initialValue: 40000
						})
						(
							<Input type='number'/>
						)}
					</Form.Item>
					</Col>
					{ (SLM1 || SLM2 || SLM3 || SLM4 || SLM5 || SLM6) ?
					<Divider>Site level mapping</Divider>
					: null}
					{SLM1 ?
					<Col span={12}>
						<Form.Item
							style={{display: 'none'}}
						>
							{getFieldDecorator('SLM1_Name', {
							})
							(
								<span hidden />
							)}
						</Form.Item>
						<Form.Item
							label={SLM1}
							{...multipleItemsLayout}
						>
							{getFieldDecorator('SLM1_Alias', {})
							(
								<Input />
							)}
						</Form.Item>
					</Col>
					: null}
					{SLM2 ?
					<Col span={12}>
						<Form.Item
							style={{display: 'none'}}
						>
							{getFieldDecorator('SLM2_Name', {
							})
							(
								<span hidden />
							)}
						</Form.Item>
						<Form.Item
							label={SLM2}
							{...multipleItemsLayout}
						>
							{getFieldDecorator('SLM2_Alias', {})
							(
								<Input />
							)}
						</Form.Item>
					</Col>
					: null}
					{SLM3 ?
					<Col span={12}>
						<Form.Item
							style={{display: 'none'}}
							{...multipleItemsLayout}
						>
							{getFieldDecorator('SLM3_Name', {
							})
							(
								<span hidden />
							)}
						</Form.Item>
						<Form.Item
							label={SLM3}
							{...multipleItemsLayout}
						>
							{getFieldDecorator('SLM3_Alias', {})
							(
								<Input />
							)}
						</Form.Item>
					</Col>
					: null}
					{SLM4 ?
					<Col span={12}>
						<Form.Item
							style={{display: 'none'}}
							{...multipleItemsLayout}
						>
							{getFieldDecorator('SLM4_Name', {
							})
							(
								<span hidden />
							)}
						</Form.Item>
						<Form.Item
							label={SLM4}
							{...multipleItemsLayout}
						>
							{getFieldDecorator('SLM4_Alias', {})
							(
								<Input />
							)}
						</Form.Item>
					</Col>
					: null}
					{SLM5 ?
					<Col span={12}>
						<Form.Item
							style={{display: 'none'}}
						>
							{getFieldDecorator('SLM5_Name', {
							})
							(
								<span hidden />
							)}
						</Form.Item>
						<Form.Item
							label={SLM5}
							{...multipleItemsLayout}
						>
							{getFieldDecorator('SLM5_Alias', {})
							(
								<Input />
							)}
						</Form.Item>
					</Col>
					: null}
					{SLM6 ?
					<Col span={12}>
						<Form.Item
							style={{display: 'none'}}
						>
							{getFieldDecorator('SLM6_Name', {
							})
							(
								<span hidden />
							)}
						</Form.Item>
						<Form.Item
							label={SLM6}
							{...multipleItemsLayout}
						>
							{getFieldDecorator('SLM6_Alias', {})
							(
								<Input />
							)}
						</Form.Item>
					</Col>
					: null}
					<Divider>SLA</Divider>
					<Col span={12}>
						<Form.Item
							{...multipleItemsLayout}
							label='Callbacks'
						>
							{getFieldDecorator('callbacks', {
								initialValue: 1
							})
							(
								<Input  type='number'/>
							)}
						</Form.Item>
					</Col>
					<Col span={12}>
					<Form.Item
						{...multipleItemsLayout}
						label='Call answer rate'
					>
						{getFieldDecorator('callAnswerRate', {
							initialValue: 1
						})
						(
							<Input  type='number'/>
						)}
					</Form.Item>
					</Col>
					<Col span={12}>
						<Form.Item
							{...multipleItemsLayout}
							label='Sessions'
						>
							{getFieldDecorator('sessions', {
								initialValue: 1
							})
							(
								<Input  type='number'/>
							)}
						</Form.Item>
					</Col>
					<Divider>Others</Divider>
					<Col span={12}>
					<Form.Item
						{...multipleItemsLayout}
						label='Assign products'
					>
						{getFieldDecorator('products', {})
						(
							<Select mode='multiple' placeholder='Choose products'>
								{productsList.map((item, index) => <Option key={index} value={item.product}>{item.product}</Option>)}
							</Select>
						)}
					</Form.Item>
					</Col>
					<Col span={12}>
					<Form.Item
						{...multipleItemsLayout}
						label='Reporting cycle - Start Date'
					>
						{getFieldDecorator('reportingCycle', {
							initialValue: moment('Jan-01', 'MMM-DD')
						})
						(
							<DatePicker format='MMM-DD' placeholder='Start Date'/>
						)}
					</Form.Item>
					</Col>
					<Col span={24}>
					<Form.Item {...tailFormItemLayout}>
						<Button type='primary' disabled={!hasPermission('/view/settings/edit-settings', PermissionsRobin.getResult('own-permissions'))} htmlType='submit'>Submit</Button>
					</Form.Item>
					</Col>
				</Form>
			</div>
		</ErrorBoundary>
	}

}

export const ClientSettingsFormPage = Form.create<ClientSettingsModel>({
	mapPropsToFields : props => {
		return _.mapValues(props, (v) => {
			return Form.createFormField({value : v})
		})
	}
})(ClientSettingsForm);
export default ClientSettingsFormPage;
