import * as React from 'react';
import {TitledCard} from '@simplus/siui'
import ClientSettingsPage from './ClientSettingsForm'
import * as moment from 'moment';
import {connectRobin} from '@simplus/robin-react'
import {robins} from '../../robins'
import {ErrorBoundary} from '../../utils/ErrorBoundary'
const {clientSettings, UsersRobin, ProductsRobin, ClientsRobin} = robins;

export interface ClientSettingsOwnProps {

}
interface State {
	form: any;
}
@connectRobin([clientSettings, UsersRobin, ProductsRobin, ClientsRobin])
export class ClientSettings extends React.Component<ClientSettingsOwnProps, State> {
	/**
	 *  ComponentDidMount method to initiate robin get dashboard containers request
	 */

	componentWillMount(): void {
		clientSettings.findOne((this.props as any).match.params.id)
		ClientsRobin.findOne((this.props as any).match.params.id)
		UsersRobin.find({})
		ProductsRobin.find({})
	}

	/**
	 *  Render method
	 */
	render(): JSX.Element {
		const default_settings = [{
			benchmarks: [{}],
			sla: {},
			siteMapping: []
		}];
		const settings:any = clientSettings.getModel() || default_settings;
		const users = UsersRobin.getCollection()
		const products = (ProductsRobin.getCollection() || [
			{product: ''},
		]).filter(product => (!!product&& (product.product!=='Absence ')))
		
		const client = ClientsRobin.getModel() || {siteMapping: []};
		let valid_settings;
		settings[0] ? valid_settings = settings[0] : valid_settings = default_settings[0]
		const SLM1 = {name: '', alias: ''};
		const SLM2 = {name: '', alias: ''};
		const SLM3 = {name: '', alias: ''};
		const SLM4 = {name: '', alias: ''};
		const SLM5 = {name: '', alias: ''};
		const SLM6 = {name: '', alias: ''};
		if (valid_settings.siteMapping[0]) {
			SLM1.name = valid_settings.siteMapping[0].name
			SLM1.alias = valid_settings.siteMapping[0].alias
		}
		if (valid_settings.siteMapping[1]) {
			SLM2.name = valid_settings.siteMapping[1].name
			SLM2.alias = valid_settings.siteMapping[1].alias
		}
		if (valid_settings.siteMapping[2]) {
			SLM3.name = valid_settings.siteMapping[2].name
			SLM3.alias = valid_settings.siteMapping[2].alias
		}
		if (valid_settings.siteMapping[3]) {
			SLM4.name = valid_settings.siteMapping[3].name
			SLM4.alias = valid_settings.siteMapping[3].alias
		}
		if (valid_settings.siteMapping[4]) {
			SLM5.name = valid_settings.siteMapping[4].name
			SLM5.alias = valid_settings.siteMapping[4].alias
		}
		if (valid_settings.siteMapping[5]) {
			SLM6.name = valid_settings.siteMapping[5].name
			SLM6.alias = valid_settings.siteMapping[5].alias
		}
		const SLM: string[] = [];
		client.siteMappings ? client.siteMappings.map((item) => SLM.push(item.siteLevel)) : SLM
		const startDate = moment(client.FinancialYearEnd).add(1, 'day')
		const reportingCycle = settings[0] ? moment(valid_settings.reportingCycle) : startDate
		const formProps = {
			client: (this.props as any).match.params.id,
			_key: valid_settings._key,
			img: valid_settings.img,
			avgWorkingHours: valid_settings.avgWorkingHours || 8,
			avgEmployeeWage: valid_settings.avgEmployeeWage || 40000,
			products: valid_settings.products,
			productsList: products,
			users: users,
			icasCustodian: valid_settings.icasCustodian,
			clientCustodian: valid_settings.clientCustodian,
			reportingCycle:reportingCycle,
			callbacks: valid_settings.sla.callbacks || '1',
			callAnswerRate: valid_settings.sla.callAnswerRate || '1',
			sessions: valid_settings.sla.sessions || '1',
			SLM1_Name: SLM1.name || SLM[0],
			SLM1_Alias: SLM1.alias || SLM[0],
			SLM2_Name: SLM2.name || SLM[1],
			SLM2_Alias: SLM2.alias || SLM[1],
			SLM3_Name: SLM3.name || SLM[2],
			SLM3_Alias: SLM3.alias || SLM[2],
			SLM4_Name: SLM4.name || SLM[3],
			SLM4_Alias: SLM4.alias || SLM[3],
			SLM5_Name: SLM5.name || SLM[4],
			SLM5_Alias: SLM5.alias || SLM[4],
			SLM6_Name: SLM6.name || SLM[5],
			SLM6_Alias: SLM6.alias || SLM[5],
		}
		return <ErrorBoundary>
			<div className='settings-form' style={{display: 'flex', justifyContent: 'center'}}>
				<TitledCard
					title={client.Name}
					titleStyle={{ background : 'rgb(43, 75, 126)'}}
					rounded
					loading={clientSettings.isLoading(clientSettings.ACTIONS.FIND_ONE)||ClientsRobin.isLoading(ClientsRobin.ACTIONS.FIND_ONE)}
					style={{width: '70%', margin: '2rem 0'}}
					>
						<ClientSettingsPage {...formProps} />
				</TitledCard>
			</div>
		</ErrorBoundary>
	}
}

export default ClientSettings