import * as React from 'react';
import {HashRouter as Router, Route, Switch} from 'react-router-dom'
import {ClientSettings} from './ClientSettings'
import {ClientsList} from './Clients'
export class ClientSettingsApp extends React.Component {
	/**
	 *  Render method
	 */
	render(): JSX.Element {
		return(
			<Router>
				<Switch>
					<Route exact path='/settings/:id' component={ClientSettings} />
					<Route exact path='/settings/' component={ClientsList} />
				</Switch>
			</Router>
		)
	}
}

export default ClientSettingsApp;