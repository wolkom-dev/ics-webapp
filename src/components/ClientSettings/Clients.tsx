import * as React from 'react'

import { Table, Icon, Tooltip } from 'antd';
import {FillLayout} from '@simplus/macaw-business'
import {Card, Input} from '@simplus/siui'
import {ClientsModel} from '../../models'
import { Link } from 'react-router-dom';
import {connectRobin} from '@simplus/robin-react'
import {robins} from '../../robins'
import {ErrorBoundary} from '../../utils/ErrorBoundary'
const Search = Input.SearchInput;
const {ClientsRobin, clientSettings} = robins;

@connectRobin([ClientsRobin, clientSettings])
export class ClientsList extends React.Component {
	state = {filter: ''}
	/**
	 *  ComponentDidMount method to initiate robin request
	 */

	componentDidMount(): void {
		ClientsRobin.find({})
		clientSettings.find({})
	}

	render(): JSX.Element {
		const { Column } = Table;
		const data = ClientsRobin.getCollection() || []
		const settings = clientSettings.getCollection() || []
		const table_data = this.state.filter ? data.filter(item => item.Name.toLocaleLowerCase().startsWith(this.state.filter.toLocaleLowerCase())) : data
		return(
			<ErrorBoundary>
				<FillLayout style={{flexGrow: 1}}>
					<Card rounded margin padding loading={ClientsRobin.isLoading(ClientsRobin.ACTIONS.FIND)||clientSettings.isLoading(clientSettings.ACTIONS.FIND)}>
						<Search label='Filter' placeholder='Type to filter results' onChange={value =>  this.setState({filter: value})}/>
						<Table style={{marginTop: '1rem'}} dataSource={table_data} rowKey={'Name'}>
								<Column
									title='Client Name'
									dataIndex='Name'
									key='Name'
								/>
								<Column
									title='Industry'
									dataIndex='Industry'
									key='Industry'
								/>
								<Column
										title='Number Of Employees'
										dataIndex='EmployeeCount'
										key='EmployeeCount'
								/>
								<Column
										title='Action'
										key='action'
										render={(client: ClientsModel) => (
											<span>
												<Link to={`/settings/${client._key}`}>
													<Tooltip title='Click To Edit Client Settings'>
														<Icon style={{fontSize: 20}} type='setting' />
													</Tooltip>
												</Link>
												{settings.find((item) => item.client === client._key) ?
													null :
													<Tooltip title='Settings not done yet'>
															<Icon style={{marginLeft: '1rem', color: 'red', fontSize: 20}} type='warning' />
													</Tooltip>
												}
											</span>
									)}
								/>
						</Table>
					</Card>
				</FillLayout>
			</ErrorBoundary>
		)
			
	}
}

export default ClientsList