import * as React from 'react'
import {BreadcrumbRouter} from './BreadCrumbs'
import {HashRouter as Router, Route} from 'react-router-dom';
import {connectRobin} from '@simplus/robin-react'
import {robins} from '../../robins'

@connectRobin([robins.AuthRobin])
export class Breadcrumbs extends React.Component{
		render() {
			return <Router>
				{robins.AuthRobin.getUserInfo()?
					<Route path='/:id' component={BreadcrumbRouter} /> :
					<Route path='/:id' component={() => <div></div>} />
				}
			</Router>
		}
}