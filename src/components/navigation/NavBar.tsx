import * as React from 'react'
import {TopMenu, TopMenuItem, Profile} from '@simplus/siui'
import {NavLink, Link, withRouter} from 'react-router-dom'
import { Menu, Icon, Dropdown } from 'antd'
import {connectRobin} from '@simplus/robin-react'
import {robins} from '../../robins'
import {hasPermission} from '../../utils'
import {ErrorBoundary} from '../../utils/ErrorBoundary'
const {AuthRobin, PermissionsRobin} = robins
const wr = withRouter as any

@wr
@connectRobin([AuthRobin, PermissionsRobin])
export class NavBar extends React.Component {
	render(): JSX.Element {
		const user = AuthRobin.getUserInfo()
		if (!user) {
			return <div></div>
		}
		const menu = (
			<Menu style={{width: '200px'}}>
				<Menu.Item disabled>
					<p style = {{float: 'left', paddingLeft: '10px', paddingTop: '10px'}}>{user.name}</p>
					<img src='/src/assets/img/icas.png' style = {{width: '60px', height: '35px', float: 'right'}} />
				</Menu.Item>
				{hasPermission('/view/user/view-profile', PermissionsRobin.getResult('own-permissions')) ?
				<Menu.Item>
					<Link to='/user'>
						<Icon type='user' /> &nbsp; &nbsp; My Profile &nbsp; &nbsp;
					</Link>
				</Menu.Item>
				: null }
				{hasPermission('/view/usermanagement', PermissionsRobin.getResult('own-permissions')) ?
				<Menu.Item>
					<a href='/security'>
						<Icon type='usergroup-add' /> &nbsp; &nbsp; User Management &nbsp; &nbsp;
					</a>
				</Menu.Item>
				: null}
				{hasPermission('/view/settings/view-settings', PermissionsRobin.getResult('own-permissions')) ?
				<Menu.Item>
					<Link to='/settings'>
						<Icon type='setting'/> &nbsp; &nbsp;Settings &nbsp; &nbsp;
					</Link>
				</Menu.Item>
				: null }
				<Menu.Item>
					<div onClick={() => AuthRobin.when(AuthRobin.logout()).then(() => (this.props as any).history.push('/'))}>
						<Icon type='logout'/> &nbsp; &nbsp;Log-Out &nbsp; &nbsp;
					</div>
				</Menu.Item>
			</Menu>
			);

		return <ErrorBoundary>
			<div><TopMenu style={{
					backgroundColor : 'white',
					paddingTop: '10px',
					height: '70px'
					}}
					picture={
					<div style={{margin: 'auto'}}>
					<a href='/'><img src={'/src/assets/img/icas.png'} alt='ICAS Logo.png' style={{width: '60px', height: '35px', marginTop: '1rem', marginBottom : '3rem', marginLeft: '1.5rem', marginRight: '1rem' }} className='si-logo'/></a>
					</div>
					}
					rightComponents={[

					<Dropdown overlay={menu}>
					<a className='ant-dropdown-link'>
					<Profile
					picture={{ rounded : true, url : user.picture || '/src/assets/img/sample-profile.png', size : 45}}
					name=''
					role=''/>
					</a>
					</Dropdown>
				]}>
				<NavLink activeClassName='active' exact to='/'>
				<TopMenuItem className='menu-item'>Home</TopMenuItem>
				</NavLink>
				<NavLink activeClassName='active' to='/analysis'>
				<TopMenuItem className='menu-item'>Analysis</TopMenuItem>
				</NavLink>
			</TopMenu></div>
		</ErrorBoundary>
	}
}