import * as React from 'react'
import {ErrorBoundary} from '../../utils/ErrorBoundary'
export interface DashboardTabProps{

}

export class DashboardTab extends React.Component<DashboardTabProps>{
	render(): JSX.Element {
		return <ErrorBoundary>
			<div className='dashboard-tab'>
			{/* <DashboardFilter filters={tab.filters}/> */}
			<table style = {{width: '100%', tableLayout: 'fixed'}}>
				<tbody>
						{React.Children.map(this.props.children, (item,index) => {
							return <tr key={index}>
								<td key={index} colSpan={1}>
									{item}
								</td>
							</tr>
						})}
				</tbody>
			</table>
		</div>
	</ErrorBoundary>
	}
}