import * as React from 'react'
import {BaseStructureHelper} from './BaseStructure'
import {ErrorBoundary} from '../../utils/ErrorBoundary'
export interface DashboardStrucutureItemSize {

}

export interface DashboardStrucuture{
	size : number
	items: number[]
}

export interface DashboardProps {
	name?: string;
	structure: DashboardStrucuture
}

export class Dashboard extends React.Component<DashboardProps>{

	render(){
		return <ErrorBoundary>
			<div>
				{this.props.name? <p style={{margin: '5px 2px', fontWeight: 400, fontSize: '1.2rem'}}>{this.props.name}</p> : null}
				{/* <DashboardFilter filters={this.props.filters}/> */}
				<table  style = {{width: '100%', tableLayout: 'fixed'}}>
					<tbody>
					{BaseStructureHelper.genStructure(this.props.structure.items, this.props.structure.size).map((row, index) => {
						return <tr key={index}>
							{row.map(col => {
								return <td key={col.index} colSpan={col.size}>{React.Children.toArray(this.props.children)[col.index]}</td>
							})}
						</tr>
					})}
					</tbody>
				</table>
			</div>
		</ErrorBoundary>
	}
}