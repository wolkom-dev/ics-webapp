export type BaseStructure = {index: number, size : number }[][]

export class BaseStructureHelper{
	static genStructure(sizes : number[], structureWidth : number): BaseStructure {
		let grid: BaseStructure = [[]]
		let col = 0
		let count = 0
		sizes.forEach( (item:number, i) => {
			if(item + count > structureWidth){
				col++; count = i;
				grid.push([{index : i, size : item}])
			} else {
				count+=item;
				grid[col].push({index : i, size : item})
			}
		})
		return grid
	}
}