import * as React from 'react'
import {Select, MultiSelect, MultiSelectOption, DatePicker, Switch} from '@simplus/siui'
import {ErrorBoundary} from '../../utils/ErrorBoundary'
export interface DashboardFilterProps {
	type: 'single' | 'multiple' | 'date' | 'switch',
	customDate?: boolean
	title?: string,
	options?: Array<{value: string, text: string}>,
	text?: string
	width?: number
	style?: React.CSSProperties
	defaultValue ?: string
	checked ?: boolean
	className ?: string
	onChange(values: any): any
}

export class DashboardFilter extends React.Component<DashboardFilterProps> {
/**
	 *  This is the render method
	 */
	render(): JSX.Element {
		let filterValue = <div/>
			switch (this.props.type) {
				case 'single':
					filterValue = <Select
						label={this.props.title}
						value={this.props.defaultValue}
						className={this.props.className}
						style={{ minWidth: this.props.width || 100 , ...this.props.style}}
						onChange={(selected) => this.props.onChange(selected.toString())}>
						{(this.props.options || [{value: '', text: ''}]).map((option, index) => {
							{
								return <Select.Option key={option.value} value={option.value}>{option.text}</Select.Option>
							}
						})}
					</Select>
					break;
				case 'multiple':
					filterValue =	<MultiSelect
						className={this.props.className}
						style={{ minWidth: this.props.width || 100 , ...this.props.style}}
						onChange={(selected) => this.props.onChange(selected)}>
						{(this.props.options || [{value: '', text: ''}]).map(option => {
							<MultiSelectOption key={option.value} value={option.value}>{option.text}</MultiSelectOption>
						})}
					</MultiSelect>
					break;
				case 'switch':
					filterValue = <Switch
						label={this.props.title}
						checked={this.props.checked}
						className={this.props.className}
						style={{ minWidth: this.props.width, marginBottom: '6px', ...this.props.style}}
						onChange={(selected) => this.props.onChange(selected)} />
					break;
				case 'date':
					filterValue =	<DatePicker
						label={this.props.title}
						customOption={this.props.customDate}
						value={this.props.defaultValue}
						className={this.props.className}
						style={{minWidth: this.props.width || 100, ...this.props.style}}
						onChange={(selected) => this.props.onChange(selected)}>
					{(this.props.options || [{value: '', text: ''}]).map(option => {
						switch (option.text) {
							case 'Past quarter':
								return <Select.Option key={option.value} value='past 90 days'>{option.text}</Select.Option>
							case 'Past year':
								return <Select.Option key={option.value} value='past year'>{option.text}</Select.Option>
						}
					})}
				</DatePicker>
				break;
			}
		return <ErrorBoundary>
				<div style={{display: 'flex', alignItems: 'flex-end'}}>
					{
						this.props.text ?
							<div className='dashboard-filter-text'>{this.props.text}</div>
						: null
					}
					{filterValue}
				</div>
			</ErrorBoundary>
	}
}