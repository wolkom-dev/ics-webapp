import * as React from 'react'
import {Card} from '@simplus/siui'
import {
	Tooltip,
	Icon,
	Tabs,
} from 'antd'
import {ErrorBoundary} from '../../utils/ErrorBoundary'
const {TabPane} = Tabs

export interface DashboardSectionModel {
	title?: string,
	tooltip?: string,
	tabTitles?: string[]
	tabTooltips?: string[]
}

export interface DashboardSectionProps {
	section: DashboardSectionModel
	onTabChange?(tab: string): void
	loading?: boolean
	defaultActiveTab?: string
}

export class DashboardSection extends React.Component<DashboardSectionProps>{
	onChange = (selected) => {
		if (this.props.onTabChange) {
			this.props.onTabChange(selected)
		}
	}
	render() {
		return <Card margin padding loading={this.props.loading}>
		<ErrorBoundary>
			<div className='dashboard-section-header'>
				{this.props.section.title ? <div className='dashboard-section-title'>{this.props.section.title}</div> : null}
				{
					this.props.section.tooltip ?
						<Tooltip title={this.props.section.tooltip}>
							<Icon style={{color: '#909090'}} type='info-circle' />
						</Tooltip>
					: null
				}
			</div>
			{React.Children.count(this.props.children) > 1 ?
				<Tabs activeKey={this.props.defaultActiveTab}
					onChange={(selected)=> this.onChange(selected)}>
					{React.Children.map(this.props.children, (item, index) => {
						return <TabPane tab={this.props.section.tabTitles ?
							(this.props.section.tabTooltips ? <Tooltip title={this.props.section.tabTooltips[index]}> {this.props.section.tabTitles[index]} </Tooltip> : this.props.section.tabTitles[index])
							: 'Tab'} key={index}>
						{item}
					</TabPane>
					})}
				</Tabs>
				: this.props.children
			}
		</ErrorBoundary>
	</Card>
	}
}