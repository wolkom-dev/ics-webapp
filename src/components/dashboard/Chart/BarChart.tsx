import * as React from 'react'
import * as _ from 'lodash'
import {BarChart as BC, XAxis, YAxis, Legend, Label, Bar, ResponsiveContainer, Tooltip, ReferenceLine} from 'recharts'
import { Loader, Button} from '@simplus/siui'
import {ErrorBoundary} from '../../../utils/ErrorBoundary'

const DEFAULT_COLORS = ['#224b81', '#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

export interface BarChartProps {
	config?: {
		colors?: string
		showLegend?: boolean
	}
	bars: string[]
	data: Array<{
		x: string
		[key: string]: string|number|number[]
	}>
	minHeight?: number
	yaxisWidth?: number
	yaxisUnits?: string
	yaxisLabel?: string
	barSize?: number
	horizontal?: boolean
	relativeToTotal?: boolean
	hideEmptyBars?: boolean
	loading?: boolean
	error?: boolean
}

export class BarChart extends React.Component<BarChartProps> {
	state={
        shrink:true
    }
    showchange= () =>{
        this.setState({shrink:!this.state.shrink})
    }
	render(): JSX.Element {
		const COLORS = _.get(this.props, 'config.colors', DEFAULT_COLORS)
		const showLegend = _.get(this.props, 'config.showLegend', false)
		let data = this.props.data
		let unit = this.props.yaxisUnits
		const emptyBars: string[] = []
		if (this.props.relativeToTotal && data && data.length > 0) {
			unit = '%'
			const totals = _.mapValues(data[0], (v, k) => _.sum(data.map( o => o[k])))
			data = data.map( e => {
				return {..._.mapKeys(e, (k, v) => `___${v}`), ..._.mapValues(e, (v, k) => {
						if (k !== 'x' && typeof v === 'number') {
							const total = totals[k]
							return (v / total) * 100
						}
						return v
				})}
			}) as any
		}
		const NO_DATA = <div style={{textAlign: 'center', margin: '20px', display: 'flex', justifyContent: 'center', alignItems: 'center', position: 'relative', height: this.props.minHeight || (this.props.horizontal ? 480 : (this.props.data || []).length * 70 + 200 )}}>
			No data
			{(this.props.loading || this.props.error) ? <Loader error={this.props.error}/> : null}
		</div>
		let negative = false
		if (data) {
			data = data.filter( e => {
				const numbers = _.values(e).filter( v => typeof v === 'number')
				const numbersAboveZero = numbers.filter( v => parseFloat((v as any).toFixed(1)) !== 0 )
				if (numbersAboveZero.length === 0) {
					emptyBars.push( _.get(e, 'x') as string )
					negative = true
				}
				return numbers.length === 0 || numbersAboveZero.length > 0
			})
			data.map(items => {
				for (const key in this.props.bars ) {
					const name = this.props.bars[key]
					items[name] = items[name] || 0;
				}
			})
			if (data.length === 0)
				return NO_DATA
			const final_data = (this.state.shrink && !this.props.horizontal) ? this.props.data.filter((i,index) => index < 10 ) : data
			return <ErrorBoundary> 
					<div style={{position : 'relative', display: 'flex', flexDirection: 'column', alignItems: 'flex-end', paddingBottom: '5px'}}>
						{this.props.loading ? <Loader/> : null}
						<ResponsiveContainer width='100%' height='100%' minHeight={this.props.minHeight || (this.props.horizontal ? 480 : (final_data || []).length * 70 + 200 )} maxHeight={this.props.minHeight || (this.props.horizontal ? 480 : (final_data || []).length * 70 + 200 )}>
							<BC data={final_data} layout={this.props.horizontal ? 'horizontal' : 'vertical'} margin={{top: 20, right: 20, bottom: 20, left: 100}} barGap={0}>
								<XAxis unit={this.props.horizontal ? '' : unit} dataKey={this.props.horizontal ? 'x' : undefined} type={this.props.horizontal ? 'category' : 'number'} domain={negative ? [0, 'auto'] : ['auto', 0]}/>
								<YAxis unit={this.props.horizontal ? unit : ''} width={this.props.yaxisWidth || 100} dataKey={this.props.horizontal ? undefined : 'x'} type={this.props.horizontal ? 'number' : 'category'} domain={negative ? [0, 'auto'] : ['auto', 0]}>
								<Label position='insideLeft'>{this.props.yaxisLabel}</Label>
								</YAxis>
								<ReferenceLine stroke='black' x={0}/>
								<ReferenceLine stroke='black' y={0}/>
								<Tooltip content={ (props) => {
									if (props.active) {
										return <div>
											{props.payload.map( (p, index) => {
												return <div key={index}>
														<h3>{p.payload.x} - {p.dataKey}</h3>
														<p ><span style={{fontWeight: 700}}>{p.payload[`___${p.dataKey}`]} ({(p.payload[p.dataKey] || 0).toFixed(2)}%)</span></p>
													</div>
											})}
										</div>
									}
								return null
								}}/>
								{showLegend === true && <Legend  verticalAlign='top' height={36} align='right' iconType='circle'/>}
								{this.props.bars.map((b: string, index) => {
									//  label={{ position: this.props.horizontal ? 'top' : 'right', formatter: (val) => `${isNaN(val) ? val : (parseInt(val)).toFixed(1)}${unit || ''}`} as any}
									return <Bar key={`${b} - ${index}`} dataKey={b} barSize={this.props.barSize || 30}  fill={COLORS[index % COLORS.length]}/>;
								})}
							</BC>
						</ResponsiveContainer>
						{data.length > 10 ? <Button style={{marginRight: '8px', padding: '10px'}} rounded onClick={this.showchange}>{this.state.shrink? 'Show all items':'Show top 10 items'}</Button> : null}
					</div>
				</ErrorBoundary>
		}

		return NO_DATA
	}
}