import * as React from 'react'
import ReactTable from 'react-table'
import {Column} from 'react-table'
import 'react-table/react-table.css'
import {ErrorBoundary} from '../../../utils/ErrorBoundary'

export interface TableChartProps<Data = any> {
	columns: Column[]
	sortable?: boolean
	loading?: boolean
	// tslint:disable-next-line
	data: Data
}

export class TableChart extends React.Component<TableChartProps> {

	render(): JSX.Element {
		if (this.props.data) {
			return <ErrorBoundary>
				<ReactTable
					loading={this.props.loading}
					sortable={this.props.sortable}
					columns={this.props.columns}
					data={this.props.data}
					defaultPageSize={10}
					className='-striped -highlight'
				/>
			</ErrorBoundary>
		}

		return <div style={{textAlign: 'center', margin: '20px'}}>No data</div>
	}
}