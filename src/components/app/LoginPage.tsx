import * as React from 'react';
import {TitledCard } from '@simplus/siui';
import {Link} from 'react-router-dom'
import {Input, Form, Icon, Button, message} from 'antd';
import {FormComponentProps} from 'antd/lib/form';
import {robins} from '../../robins'
import {ErrorBoundary} from '../../utils/ErrorBoundary'
const Item = Form.Item;

/**
 *  Login Page
 */

export const LoginPage = Form.create()(class Login extends React.Component<FormComponentProps> {
	handleSubmit = (evt) => {
		evt.preventDefault();

		const email = this.props.form.getFieldValue('email')
		const password = this.props.form.getFieldValue('password')
		robins.AuthRobin.when(robins.AuthRobin.login(email, password)).then( data => {
			message.success('You are now logged in !')
			robins.PermissionsRobin.get('own-permissions', '/')
		}).catch( err => {
			message.error('Wrong username or password !')
		})
	}

	render(): JSX.Element {
		const { getFieldDecorator,  getFieldError } = this.props.form;
		const passwordError = getFieldError('password')
		const emailError = getFieldError('email')

		return <ErrorBoundary>
			<div className='si-background-cover mcw-bizz-auth-layout' style={{
				backgroundImage: 'url(/src/assets/img/LoginBG.png)', backgroundSize: 'cover', marginTop: '-140px',
				display : 'flex',
				justifyContent : 'center',
				alignItems : 'center',
				flexGrow: 1
			}}>
				<TitledCard
					title={<img src='/src/assets/img/icas.png' width={300} alt='Logo'/> }
					titleStyle={{background: 'white', height: 230}}
					loading={robins.AuthRobin.isLoading(robins.AuthRobin.ACTIONS.LOGIN)||robins.PermissionsRobin.isLoading('own-permissions')}
					style={{
						width : '500px',
						display : 'flex',
						justifyContent : 'center',
						alignItems : 'center',
						flexDirection: 'column',
						padding: '0 3rem'
					}}
					>
					<Form onSubmit={this.handleSubmit} className='login-form' style={{padding : '0 3rem'}}>
							<Item style={{ marginBottom : '1rem'}}>
							{getFieldDecorator('email', {
								rules: [
									{ required: true, message: 'Please input your email!' },
								],
							})(
								<Input type={'email'} prefix={<Icon type='user' style={{ fontSize: 13 }} />} placeholder='Email' />
							)}
							</Item>
							<Item style={{ marginBottom : '1rem'}}>
							{getFieldDecorator('password', {
								rules: [{ required: true, message: 'Please input your Password!' }],
							})(
								<Input prefix={<Icon type='lock' style={{ fontSize: 13 }} />} type='password' placeholder='Password' />
							)}
							</Item>
							<div style={{ padding: '1rem 0'}}>
								<Link to='/reset-password'>Forgot your password ?</Link>
							</div>
							<div className='mcw-biz-auth-btn-row'>
								<Button  disabled={!!(emailError || passwordError)} style={{ height : '4rem', width : '150px', marginTop: '1rem'}} type='primary' htmlType='submit' className='login-form-button shadow'>
									Log in
								</Button>
							</div>
						</Form>
						<div style={{ textAlign : 'center' }}>
							<a href='/oauth/google'><Button  style={{ height : '4rem', width : '150px', marginTop: '1rem'}}>Log in Google</Button></a>
						</div>
				</TitledCard>
			</div>
		</ErrorBoundary>
	}
})
