import * as React from 'react';
import {HashRouter as Router, Route, Switch} from 'react-router-dom'
import {AnalysisApp} from '../../components/analysis/index'
import {ClientSettingsApp} from '../../components/ClientSettings'
import {ProfileApp} from '../../components/usermanagement/pages/Profile'
import {UsermanagementApp} from '../../components/usermanagement'
import {Welcome} from './homeCard';
import {LoginPage} from './LoginPage';
import {SetPassword} from './SetPassword';
import {ResetPassword} from './ResetPassword';
import {robins} from '../../robins'
import {connectRobin} from '@simplus/robin-react'
import {ErrorBoundary} from '../../utils/ErrorBoundary'
import { Loader } from '@simplus/siui';
const {AuthRobin, PermissionsRobin} = robins

@connectRobin([AuthRobin, PermissionsRobin])
export class App extends React.Component {
	componentWillMount(): void {
		robins.AuthRobin.when(robins.AuthRobin.loadUserInfo()).then( key => {})
	}

	componentDidMount(): void {
		PermissionsRobin.get('own-permissions', '/')
	}
	/**
	 *  Render method
	 */
	render(): JSX.Element {
		return <div className='app-container' style={{position: 'relative'}}>
			<ErrorBoundary>
				{AuthRobin.isLoading(AuthRobin.ACTIONS.LOGOUT) ? <Loader /> : null}
				<Router>
					{AuthRobin.getUserInfo() ? <Switch>
						<Route exact path='/' component={Welcome} />,
						<Route exact path='/login' component={LoginPage} />,
						<Route path='/analysis' component={AnalysisApp} />,
						<Route path='/settings' component={ClientSettingsApp} />,
						<Route path='/user' component={ProfileApp} />,
						<Route path='/usermanagement' component={UsermanagementApp} />
						<Route exact path='/set-password/:token' component={SetPassword} />,
					</Switch> : (
						AuthRobin.isLoading(AuthRobin.ACTIONS.FETCH_USER_INFO) ? null :
						<Switch>
							<Route exact path='/reset-password' component={ResetPassword} />,
							<Route exact path='/set-password/:token' component={SetPassword} />,
							<Route path='/' component={LoginPage} />
						</Switch>
					)}
				</Router>
			</ErrorBoundary>
		</div>
	}
}
export default App;