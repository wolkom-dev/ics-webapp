import * as React from 'react';
import {TitledCard } from '@simplus/siui';
import {Input, Form, Icon, Button, notification, Alert} from 'antd';
import {FormComponentProps, } from 'antd/lib/form';
import {robins} from '../../robins'
import {Link} from 'react-router-dom'
import {ErrorBoundary} from '../../utils/ErrorBoundary'
const Item = Form.Item;

/**
 *  Login Page
 */

export const ResetPassword = Form.create()(class extends React.Component<FormComponentProps> {
	handleSubmit = (evt) => {
		evt.preventDefault();

		const email = this.props.form.getFieldValue('email')
		robins.AuthRobin.when(robins.AuthRobin.get('reset-password', `/reset-password/${email}`)).then( data => {
			notification.success({ description : 'You should receive an email in the next minutes !', message : 'Success'})
			robins.PermissionsRobin.get('own-permissions', '/')
		}).catch( err => {
			notification.error({ message : 'Error', description: 'An error occured while trying to send you the reset email.'})
		})
	}

	render(): JSX.Element {
		const { getFieldDecorator,  getFieldError } = this.props.form;
		const emailError = getFieldError('email')
		const formItemLayout = {
			wrapperCol: {
				xs: {
					span: 24,
					offset: 0,
				}
			}
		};
		return <ErrorBoundary>
				<div className='si-background-cover mcw-bizz-auth-layout' style={{
					backgroundImage: 'url(/src/assets/img/LoginBG.png)', backgroundSize: 'cover', marginTop: '-140px',
					display : 'flex',
					justifyContent : 'center',
					alignItems : 'center',
					flexGrow: 1
				}}>
				<TitledCard
					loading={robins.AuthRobin.isLoading('reset-password')}
					title={<img src='/src/assets/img/icas.png' width={300} alt='Logo'/> }
					titleStyle={{background: 'white', height: 230}}
					style={{
						width : '500px',
						display : 'flex',
						justifyContent : 'center',
						alignItems : 'center',
						flexDirection: 'column',
						padding: '0 3rem'
					}}
					>
					<Form onSubmit={this.handleSubmit} className='login-form' style={{padding : '0 3rem'}}>
							<div style={{ paddingBottom  : '2rem'}}>
								<Alert
									message='Next steps'
									description='Enter your email address to update your password.'
									type='info'
									showIcon
									/>
							</div>
							<Item style={{ marginBottom : '1rem'}} {...formItemLayout}>
							{getFieldDecorator('email', {
								rules: [
									{ required: true, message: 'Please input your email!' },
								],
							})(
								<Input type={'email'} prefix={<Icon type='user' style={{ fontSize: 13 }} />} placeholder='Email' />
							)}
							</Item>
							<div style={{ padding: '1rem 0'}}>
								<Link to='/login'>Back to login</Link>
							</div>
							<div className='mcw-biz-auth-btn-row'>
								<Button  disabled={!!(emailError)} style={{ height : '4rem', width : '150px', marginTop:'1rem'}} type='primary' htmlType='submit' className='login-form-button shadow'>
									Send email
								</Button>
							</div>
						</Form>
				</TitledCard>
			</div>
		</ErrorBoundary>
	}
})
