import * as React from 'react';
import {Card} from '@simplus/siui';
import {FillLayout} from '@simplus/macaw-business'
import { Link } from 'react-router-dom';
import { Icon } from 'antd';
import {ErrorBoundary} from '../../utils/ErrorBoundary'

/**
 *  Welcome Message
 */
export function Welcome(): JSX.Element {
	return (
		<ErrorBoundary>
			<FillLayout style={{flexGrow: 1, backgroundColor: '#F9FBFF', backgroundImage: 'url(/src/assets/img/BG.jpg)', backgroundSize: 'cover', marginTop: '-70px'}}>
					<Card style={{display: 'flex', margin: '100px', flexDirection: 'column', justifyContent: 'center'}} padding rounded>
						<div style={{display: 'flex', alignItems: 'center'}}>
							<img src='/src/assets/img/icas.png' alt='Icas Logo' width='500px' height='auto' />
							<div style={{marginRight: '50px'}}>
									<h1 style={{color: '#0090cd', fontSize: '36px'}}>Welcome to the ICAS Employee Health & Wellness Platform </h1>
									<div style={{color: '#999999', marginTop: '20px'}}>
										<h3>Through the tailored application of our data intelligence, technology, experience and our approach, ICAS delivers quality people-focused solutions that contribute to the value of your workforce.</h3>
									</div>
							</div>
						</div>
						<Link style={{alignSelf: 'flex-end', justifySelf: 'flex-end', fontSize: '24px', marginRight: '50px'}} to='/analysis'>
							<div>Get started <Icon type='arrow-right' /></div>
						</Link>
					</Card>
			</FillLayout>
		</ErrorBoundary>
	)
}
