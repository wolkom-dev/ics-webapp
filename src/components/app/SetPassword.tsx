import * as React from 'react';
import {TitledCard } from '@simplus/siui';
import {connectRobin } from '@simplus/robin-react';
import {Input, Form, Icon, Button, notification} from 'antd';
import {FormComponentProps, } from 'antd/lib/form';
import {robins} from '../../robins'
import {withRouter, RouteComponentProps } from 'react-router-dom'
import {ErrorBoundary} from '../../utils/ErrorBoundary'
import * as owasp from 'owasp-password-strength-test'

const Item = Form.Item;

/**
 *  Login Page
 */
export const SetPassword = withRouter(connectRobin([robins.UsersRobin])(Form.create()(class extends React.Component<FormComponentProps&RouteComponentProps<any>, { sent: boolean}> {

	constructor(props) {
		super(props)
		this.state = {
			sent : false
		}
	}

	handleSubmit = (evt) => {
		evt.preventDefault();
		const password = this.props.form.getFieldValue('password')
		robins.UsersRobin.when( robins.UsersRobin.post('set-password', '/set-password/'+this.props.match.params.token, {password})).then( key => {
			notification.success({
				message: 'Password changed',
				description: 'Your password has been changed successfully.'
			})
			this.props.history.push('/')
		}).catch(err => {
			notification.error({
				message: 'Update password error',
				description: 'An error has occured while you changed your password.'
			})
		})
		this.setState({sent: true})
	}

	render(): JSX.Element {
		const { getFieldDecorator,  getFieldError, getFieldValue } = this.props.form;
		const passwordError = getFieldError('password')
		const password2Error = getFieldError('password-confirm')

		return <ErrorBoundary>
				<div className='si-background-cover mcw-bizz-auth-layout' style={{
				backgroundImage: 'url(/src/assets/img/LoginBG.png)', backgroundSize: 'cover', marginTop: '-140px',
				display : 'flex',
				justifyContent : 'center',
				alignItems : 'center',
				flexGrow: 1
			}}>
				<TitledCard
					loading={robins.AuthRobin.isLoading('set-password')}
					title={<img src='/src/assets/img/icas.png' width={300} alt='Logo'/> }
					titleStyle={{background: 'white', height: 230}}
					style={{width : '500px'}}
				>
					<Form onSubmit={this.handleSubmit} className='login-form' style={{padding : '0 3rem'}}>
							<Item style={{ marginBottom : '1rem'}}>
							{getFieldDecorator('password', {
								rules: [
									{ required: true, message: 'Please input your Password!' },
									{ validator: (rule, value, done) => {
										owasp.config({minLength : 8})
										const res = owasp.test(value)
										console.log(res)
										if (res.strong) {
											done()
										} else {
											done(res.errors[0])
										}
									} }
								],
							})(
								<Input prefix={<Icon type='lock' style={{ fontSize: 13 }} />} type='password' placeholder='Password' />
							)}
							{getFieldDecorator('password-confirm', {
								rules: [{ required: true, message: 'Please confirm your Password!' },
									{validator : (rule, value, cb) => {
										if (value === getFieldValue('password'))
											cb()
										else
											cb(rule || new Error('Passwords do not match!'))
									}, message : 'Passwords do not match!'}],
							})(
								<Input prefix={<Icon type='lock' style={{ fontSize: 13 }} />} type='password' placeholder='Password confirm' />
							)}
							</Item>
							<div className='mcw-biz-auth-btn-row'>
								<Button  disabled={!!(password2Error || passwordError)} style={{ height : '4rem', width : '150px', marginTop:'1rem'}} type='primary' htmlType='submit' className='login-form-button shadow'>
									Set password
								</Button>
							</div>
						</Form>
				</TitledCard>
			</div>
		</ErrorBoundary>
	}
})))
