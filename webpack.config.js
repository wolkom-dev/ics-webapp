const path = require('path');
const fs = require('fs');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const HtmlWebpackPlugin = require('html-webpack-plugin')
const webpack = require("webpack")
const CopyWebpackPlugin = require('copy-webpack-plugin')

const IS_PROD = (process.env.NODE_ENV+'').trim() === 'production';

console.log("[ENV] ------>",IS_PROD? "PRODUCTION":"DEVELOPMENT")

module.exports = {
    target : "web",
    devtool : IS_PROD ? false : "inline-source-map",
    entry : { 
        app : path.join(__dirname, 'src', 'index.tsx'),
    },
    resolve : {
        extensions : ['.ts','.tsx','.js','.less','.css'],
        modules : [
            path.resolve("./"),
            path.resolve("./node_modules"),
        ],
        aliasFields: ["browser"]
    },
    module : {
        rules : [
            {
                test : /\.(ts|tsx)$/,
                exclude : /node_modules/,
                include: __dirname,
                loader : 'ts-loader'
            },{
                test : /\.js$/,
                exclude : /node_modules/,
                include: __dirname,
                loader : 'babel-loader'
            },{
                test : /\.html$/,
                exclude : /node_modules/,
                include: __dirname,
                loader : 'html-loader'
            },{
                test : /\.(less|css)$/,
                loader : `style-loader!css-loader!less-loader?${JSON.stringify({ modifyVars : require("./package.json").theme, javascriptEnabled: true})}`
            }
        ]
    },
    optimization : {
        minimize : IS_PROD,
        splitChunks: {
            chunks: "all",
            minSize: 30000,
            minChunks: 1,
            maxAsyncRequests: 5,
            maxInitialRequests: 3,
            name: true,
            cacheGroups: {
                default: {
                    minChunks: 1,
                    priority: -20,
                    reuseExistingChunk: true,
                },
               simplus : {
                   minChunks : 1,
                   name : "simplus",
                   priority : -10,
                   test: /node_modules\/siui/
               }
            }
        }
    },
    plugins : [
        new webpack.DefinePlugin({
            "process.env": {
                NODE_ENV : JSON.stringify("development")
            }
        }),
        new HtmlWebpackPlugin({
            template : path.join(__dirname, 'src','assets','index.html')
        }),
        new CopyWebpackPlugin([ {from : "src/assets", to : path.join(__dirname,"dist","src","assets"), toType : "dir"} ], {})
    ]
}

if((process.env.NODE_ENV + " ").trim() === 'development') {
    module.exports.devtool = "source-map"
}