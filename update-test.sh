#!/bin/sh
docker build -f ./Dockerfile-test -t icas:webapp-test .;
docker stop webapp-test;
docker rm webapp-test;
docker run --network micro --name webapp-test -d --restart always icas:webapp-test;